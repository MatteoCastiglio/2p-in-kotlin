package it.unibo.tuprolog.solve.libs.ml.primitives.test

import it.unibo.tuprolog.solve.SolverFactory
import it.unibo.tuprolog.solve.SolverTest

interface TestMLPrimitives : SolverTest {
    companion object {
        fun prototype(solverFactory: SolverFactory): TestMLPrimitives =
            TestMLPrimitivesImpl(solverFactory)
    }

    fun testBuiltinApi()
    fun testInputLayer()
    fun testDenseLayer()
    fun testOutputLayer()
    fun testBuildNetworkKO()
    fun testBuildNetworkOK()
    fun testLoadDataSetFromCSV()
    fun testLoadSchemaFromTheory()
    fun testLoadSchemaFromTheoryKO()
    fun testAttributesSchema()
    fun testSchemaTransformation()
    fun testTransform()
    fun testTransformWithVar()
    fun testTransformProcess()
    fun testTransformNoFitError()
    fun testTheoryFromDataset()
    fun testPredictDataset()
    fun testPredict()
    fun testClassify()
    fun testPredictWithVar()
    fun testClassifyWithVar()
    fun testLoadDataSetFromTheory()
    fun testRandomSplit()
    fun testTraining()
    fun testAccuracy()
    fun testTraining4()
    fun testMSE()
    fun testEnhance()
}
