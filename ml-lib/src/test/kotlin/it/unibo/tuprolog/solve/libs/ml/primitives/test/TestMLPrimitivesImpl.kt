package it.unibo.tuprolog.solve.libs.ml.primitives.test

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.dsl.theory.prolog
import it.unibo.tuprolog.solve.SolverFactory
import it.unibo.tuprolog.solve.assertHasPredicateInAPI
import it.unibo.tuprolog.solve.assertSolverSolutionsCorrect
import it.unibo.tuprolog.solve.library.Libraries
import it.unibo.tuprolog.solve.libs.ml.MLLib
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingDataSetPrimitives.LoadDataSetFromCSVTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingDataSetPrimitives.LoadDataSetFromTheoryTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingDataSetPrimitives.RandomSplitTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingDataSetPrimitives.TransformNoFitErrorTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingNetworkPrimitives.BuildNetworkTestingKO
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingNetworkPrimitives.BuildNetworkTestingOK
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingNetworkPrimitives.ClassifyTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingNetworkPrimitives.ClassifyWithVarTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingNetworkPrimitives.DenseLayerTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingNetworkPrimitives.InputLayerTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingNetworkPrimitives.OutputLayerTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingNetworkPrimitives.PredictDatasetTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingNetworkPrimitives.PredictTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingNetworkPrimitives.PredictWithVarTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingSchemaPrimitives.AttributesSchemaTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingSchemaPrimitives.EnhanceTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingSchemaPrimitives.SchemaByTheoryTestingKO
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingSchemaPrimitives.SchemaByTheoryTestingOk
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingSchemaPrimitives.SchemaTrasformationTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingSchemaPrimitives.TheoryFromDatasetTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingSchemaPrimitives.TransformProcessTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingSchemaPrimitives.TransformTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingSchemaPrimitives.TransformWithVarTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingTrainingPrimitives.AccuracyTesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingTrainingPrimitives.MSETesting
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingTrainingPrimitives.Train4Testing
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestingTrainingPrimitives.TrainTesting
import it.unibo.tuprolog.core.List as LogicList

class TestMLPrimitivesImpl(private val solverFactory: SolverFactory) : TestMLPrimitives {

    override fun testInputLayer() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(Libraries.of(MLLib)),
                InputLayerTesting,
                mediumDuration,
                ::assertSolutionEqualsDL
            )
        }
    }

    override fun testDenseLayer() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(Libraries.of(MLLib)),
                DenseLayerTesting,
                mediumDuration,
                ::assertSolutionEqualsDL
            )
        }
    }

    override fun testOutputLayer() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(Libraries.of(MLLib)),
                OutputLayerTesting,
                mediumDuration,
                ::assertSolutionEqualsDL
            )
        }
    }

    override fun testBuildNetworkKO() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(Libraries.of(MLLib)),
                BuildNetworkTestingKO,
                mediumDuration,
                ::assertSolutionEqualsDL
            )
        }
    }

    override fun testBuildNetworkOK() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(Libraries.of(MLLib)),
                BuildNetworkTestingOK,
                longDuration,
                ::assertSolutionEqualsDL
            )
        }
    }

    override fun testLoadDataSetFromCSV() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                LoadDataSetFromCSVTesting,
                longDuration,
                ::assertSolutionEqualsDL
            )
        }
    }

    override fun testLoadDataSetFromTheory() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib),
                    staticKb = theoryOf(
                        fact { "iris"("petal_width"(5.1), "petal_length"(3.5), "sepal_width"(1.4), "sepal_length"(.2), "class"("setosa")) },
                        fact { "iris"("petal_width"(4.9), "petal_length"(3), "sepal_width"(1.4), "sepal_length"(.2), "class"("setosa")) }
                    )
                ),
                LoadDataSetFromTheoryTesting,
                longDuration,
                ::assertSolutionEqualsDL
            )
        }
    }

    override fun testRandomSplit() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                RandomSplitTesting,
                longDuration,
                ::assertSolutionEqualsDL
            )
        }
    }

    override fun testTraining() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                TrainTesting,
                longDuration,
                ::assertSolutionEqualsDL
            )
        }
    }
    override fun testAccuracy() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                AccuracyTesting,
                longDuration,
                ::assertSolutionEqualsSoft
            )
        }
    }

    override fun testTraining4() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                Train4Testing,
                longDuration,
                ::assertSolutionEqualsDL
            )
        }
    }

    override fun testMSE() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                MSETesting,
                longDuration,
                ::assertSolutionEqualsSoft
            )
        }
    }

    override fun testEnhance() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                EnhanceTesting,
                longDuration,
                ::assertSolutionEqualsSoft
            )
        }
    }

    override fun testLoadSchemaFromTheory() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib),
                    staticKb = theoryOf(
                        fact { "attribute"(0, "petal_length", "type"("integer")) },
                        fact { "attribute"(1, "petal_width", "type"("integer")) },
                        fact { "attribute"(2, "sepal_length", "type"("integer")) },
                        fact { "attribute"(3, "sepal_width", "type"("integer")) },
                        fact { "attribute"(4, "class", "type"("categorical"), LogicList.of(Atom.of("setosa"), Atom.of("versicolor"))) }
                    )
                ),
                SchemaByTheoryTestingOk,
                longDuration,
                ::assertSolutionEqualsDL
            )
        }
    }

    /**
     1- incomplete sequence
     2- not valid type
     3- duplicate number in the sequence
     4- duplicate type(index)
     **/
    override fun testLoadSchemaFromTheoryKO() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib),
                    staticKb = theoryOf(
                        fact { "attribute"(0, "petal_length", "type"("integer")) },
                        fact { "attribute"(2, "sepal_length", "type"("integer")) },
                        fact { "attribute"(3, "sepal_width", "type"("integer")) },
                        fact { "attribute"(7, "class", "type"("categorical"), LogicList.of(Atom.of("setosa"), Atom.of("versicolor"))) }
                    )
                ),
                SchemaByTheoryTestingKO,
                longDuration,
                ::assertSolutionEqualsDL
            )
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib),
                    staticKb = theoryOf(
                        fact { "attribute"(0, "petal_length", "type"("jjj")) },
                        fact { "attribute"(1, "petal_width", "type"("integer")) },
                        fact { "attribute"(2, "sepal_length", "type"("integer")) },
                        fact { "attribute"(3, "sepal_width", "type"("integer")) },
                        fact { "attribute"(4, "class", "type"("categorical"), LogicList.of(Atom.of("setosa"), Atom.of("versicolor"))) }
                    )
                ),
                SchemaByTheoryTestingKO,
                longDuration,
                ::assertSolutionEqualsDL
            )
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib),
                    staticKb = theoryOf(
                        fact { "attribute"(1, "petal_length", "type"("jjj")) },
                        fact { "attribute"(1, "petal_width", "type"("integer")) },
                        fact { "attribute"(2, "sepal_length", "type"("integer")) },
                        fact { "attribute"(3, "sepal_width", "type"("integer")) },
                        fact { "attribute"(4, "class", "type"("categorical"), LogicList.of(Atom.of("setosa"), Atom.of("versicolor"))) }
                    )
                ),
                SchemaByTheoryTestingKO,
                longDuration,
                ::assertSolutionEqualsDL
            )
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib),
                    staticKb = theoryOf(
                        fact { "attribute"(1, "petal_length", "type"("index")) },
                        fact { "attribute"(1, "petal_width", "type"("index")) },
                        fact { "attribute"(2, "sepal_length", "type"("integer")) },
                        fact { "attribute"(3, "sepal_width", "type"("integer")) },
                        fact { "attribute"(4, "class", "type"("categorical"), LogicList.of(Atom.of("setosa"), Atom.of("versicolor"))) }
                    )
                ),
                SchemaByTheoryTestingKO,
                longDuration,
                ::assertSolutionEqualsDL
            )
        }
    }

    override fun testAttributesSchema() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                AttributesSchemaTesting,
                longDuration,
                ::assertSolutionEqualsDL
            )
        }
    }

    override fun testSchemaTransformation() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                SchemaTrasformationTesting,
                longDuration,
                ::assertSolutionEqualsDL
            )
        }
    }

    override fun testTransform() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                TransformTesting,
                longDuration,
                ::assertSolutionEqualsDL
            )
        }
    }

    override fun testTransformWithVar() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                TransformWithVarTesting,
                longDuration,
                ::assertSolutionEqualsDL
            )
        }
    }
    override fun testTransformProcess() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                TransformProcessTesting,
                longDuration,
                ::assertSolutionEqualsDL
            )
        }
    }

    override fun testTransformNoFitError() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                TransformNoFitErrorTesting,
                longDuration,
                ::assertSolutionEqualsDL
            )
        }
    }

    override fun testTheoryFromDataset() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                TheoryFromDatasetTesting,
                longDuration,
                ::assertSolutionEqualsDL
            )
        }
    }

    override fun testPredictDataset() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                PredictDatasetTesting,
                longDuration,
                ::assertSolutionEqualsDL
            )
        }
    }

    override fun testPredict() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                PredictTesting,
                longDuration,
                ::assertSolutionEqualsSoft
            )
        }
    }

    override fun testClassify() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                ClassifyTesting,
                longDuration,
                ::assertSolutionEqualsSoft
            )
        }
    }

    override fun testPredictWithVar() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                PredictWithVarTesting,
                longDuration,
                ::assertSolutionEqualsSoft
            )
        }
    }

    override fun testClassifyWithVar() {
        prolog {
            assertSolverSolutionsCorrect(
                solverFactory.solverOf(
                    libraries = Libraries.of(MLLib)
                ),
                ClassifyWithVarTesting,
                longDuration,
                ::assertSolutionEqualsSoft
            )
        }
    }

    override fun testBuiltinApi() {
        prolog {
            val solver = solverFactory.solverOf(Libraries.of(MLLib))
            with(solver) {
                assertHasPredicateInAPI("input_layer", 2)
                assertHasPredicateInAPI("dense_layer", 4)
                assertHasPredicateInAPI("output_layer", 5)
                assertHasPredicateInAPI("network", 2)
                assertHasPredicateInAPI("load_dataset_from_csv", 2)
                assertHasPredicateInAPI("schema_from_theory", 1)
                assertHasPredicateInAPI("schema_attributes", 2)
                assertHasPredicateInAPI("normalize", 3)
                assertHasPredicateInAPI("one_hot_encoding", 3)
                assertHasPredicateInAPI("delete_columns", 3)
                assertHasPredicateInAPI("transform_process", 2)
                assertHasPredicateInAPI("transform", 3)
                assertHasPredicateInAPI("transform_dataset", 4)
                assertHasPredicateInAPI("fit", 2)
                assertHasPredicateInAPI("predict_dataset", 4)
                assertHasPredicateInAPI("predict", 3)
                assertHasPredicateInAPI("classify", 4)
                assertHasPredicateInAPI("train", 5)
                assertHasPredicateInAPI("mse", 4)
                assertHasPredicateInAPI("accuracy", 5)
                assertHasPredicateInAPI("enhance", 4)
                assertHasPredicateInAPI("random_split", 4)
            }
        }
    }
}
