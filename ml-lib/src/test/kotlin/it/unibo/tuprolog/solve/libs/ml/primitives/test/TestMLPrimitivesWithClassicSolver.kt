package it.unibo.tuprolog.solve.libs.ml.primitives.test

import it.unibo.tuprolog.solve.ClassicSolverFactory
import it.unibo.tuprolog.solve.SolverFactory
import kotlin.test.Test

class TestMLPrimitivesWithClassicSolver : TestMLPrimitives, SolverFactory by ClassicSolverFactory {

    private val prototype = TestMLPrimitives.prototype(this)

    @Test
    override fun testBuiltinApi() {
        prototype.testBuiltinApi()
    }

    @Test
    override fun testInputLayer() {
        prototype.testInputLayer()
    }

    @Test
    override fun testDenseLayer() {
        prototype.testDenseLayer()
    }

    @Test
    override fun testOutputLayer() {
        prototype.testOutputLayer()
    }

    @Test
    override fun testBuildNetworkKO() {
        prototype.testBuildNetworkKO()
    }

    @Test
    override fun testBuildNetworkOK() {
        prototype.testBuildNetworkOK()
    }
    @Test
    override fun testLoadDataSetFromCSV() {
        prototype.testLoadDataSetFromCSV()
    }
    @Test
    override fun testLoadSchemaFromTheory() {
        prototype.testLoadSchemaFromTheory()
    }
    @Test
    override fun testLoadSchemaFromTheoryKO() {
        prototype.testLoadSchemaFromTheoryKO()
    }
    @Test
    override fun testAttributesSchema() {
        prototype.testAttributesSchema()
    }
    @Test
    override fun testSchemaTransformation() {
        prototype.testSchemaTransformation()
    }

    @Test
    override fun testTransform() {
        prototype.testTransform()
    }

    @Test
    override fun testTransformWithVar() {
        prototype.testTransformWithVar()
    }

    @Test
    override fun testTransformProcess() {
        prototype.testTransformProcess()
    }

    @Test
    override fun testTransformNoFitError() {
        prototype.testTransformNoFitError()
    }

    @Test
    override fun testTheoryFromDataset() {
        prototype.testTheoryFromDataset()
    }
    @Test
    override fun testPredictDataset() {
        prototype.testPredictDataset()
    }
    @Test
    override fun testLoadDataSetFromTheory() {
        prototype.testLoadDataSetFromTheory()
    }

    @Test
    override fun testRandomSplit() {
        prototype.testRandomSplit()
    }
    @Test
    override fun testTraining() {
        prototype.testTraining()
    }
    @Test
    override fun testAccuracy() {
        prototype.testAccuracy()
    }
    @Test
    override fun testTraining4() {
        prototype.testTraining4()
    }
    @Test
    override fun testMSE() {
        prototype.testMSE()
    }

    @Test
    override fun testEnhance() {
        prototype.testEnhance()
    }

    @Test
    override fun testPredict() {
        prototype.testPredict()
    }
    @Test
    override fun testClassify() {
        prototype.testClassify()
    }

    @Test
    override fun testPredictWithVar() {
        prototype.testPredictWithVar()
    }

    @Test
    override fun testClassifyWithVar() {
        prototype.testClassifyWithVar()
    }
}
