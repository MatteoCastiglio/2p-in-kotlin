package it.unibo.tuprolog.solve.libs.ml.examples

import it.unibo.tuprolog.dsl.theory.prolog
import it.unibo.tuprolog.solve.Solution
import it.unibo.tuprolog.solve.Solver
import it.unibo.tuprolog.solve.classicWithDefaultBuiltins
import it.unibo.tuprolog.solve.library.Libraries
import it.unibo.tuprolog.solve.libs.ml.MLLib
import it.unibo.tuprolog.solve.libs.ml.TestCSV
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.mc
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.tempPath

fun main() {
    prolog {
        val solver = Solver.classicWithDefaultBuiltins(
            libraries = Libraries.of(
                MLLib
            ),
            staticKb = theoryOf(
                fact { mc(19,	"female",	27.9,	0,	"yes",	"southwest", 16884.924) },
                fact { "attribute"(0, "age", "type"("integer")) },
                fact { "attribute"(1, "sex", "type"("categorical"), listOf("female", "male")) },
                fact { "attribute"(2, "bmi", "type"("double")) },
                fact { "attribute"(3, "children", "type"("integer")) },
                fact { "attribute"(4, "smoker", "type"("categorical"), listOf("yes", "no")) },
                fact { "attribute"(5, "region", "type"("categorical"), listOf("northeast", "southeast", "southwest", "northwest")) },
                fact { "attribute"(6, "cost", "type"("double")) },
                rule {
                    "build_network"(A) impliedBy
                        tupleOf(
                            "input_layer"(11, I),
                            "dense_layer"(I, 80, "activation"("relu"), D),
                            "dense_layer"(D, 40, "activation"("relu"), X),
                            "dense_layer"(X, 40, "activation"("relu"), Y),
                            "output_layer"(Y, 1, "activation"("relu"), "loss"("mse"), O),
                            "network"(O, N),
                            "assert"("registered"(A, N))
                        )
                },
                rule {
                    "train"(N, A, B) impliedBy
                        tupleOf(
                            "schema_from_theory"(S),
                            "normalize"(S, listOf("age", "children", "bmi"), "S2"),
                            "one_hot_encoding"("S2", listOf("sex", "smoker", "region"), "S3"),
                            "transform_process"("S3", T),
                            "load_dataset_from_csv"(TestCSV.medicalCost, D),
                            "fit"(T, D),
                            "transform_dataset"(D, T, tempPath, O),
                            "random_split"(O, 0.9, "Train", "Test"),
                            "mse"(N, "Test", 11, A),
                            "train"(N, "Train", 11, 100),
                            "mse"(N, "Test", 11, B),
                        )
                },
                rule {
                    "build_tp"(A) impliedBy
                        tupleOf(
                            "schema_from_theory"(S),
                            "normalize"(S, listOf("age", "children", "bmi"), "S2"),
                            "one_hot_encoding"("S2", listOf("sex", "smoker", "region"), "S3"),
                            "delete_columns"("S3", listOf("cost"), "S4"),
                            "transform_process"("S4", T),
                            "load_dataset_from_csv"(TestCSV.medicalCost, D),
                            "fit"(T, D),
                            "assert"("registered"(A, T))

                        )
                },
            )
        )

        val query = tupleOf(
            "build_network"("network"),
            "build_tp"("tp"),
            "registered"("network", N),
            "registered"("tp", T),
            "enhance"("medical_cost", T, N, "cost"),
            "train"(N, A, B),
            mc(19,	"female",	27.9,	0,	"yes",	"southwest", X)

        )
        // val query = "iris"("petal_width"(5.1), "petal_length"(3), "sepal_width"(1.4), "sepal_length"(.2), "Label")
        // val query = "iris"(A, B, "sepal_width"(1.4), "sepal_length"(.2), "Label")

        solver.solve(query).forEach {
            when (it) {
                is Solution.No -> println("no.\n")
                is Solution.Yes -> {
                    println("yes: ${it.solvedQuery}")
                    for (assignment in it.substitution) {
                        println("\t${assignment.key} / ${assignment.value}")
                    }
                    println()
                }
                is Solution.Halt -> {
                    println("halt: ${it.exception.message}")
                    for (err in it.exception.prologStackTrace) {
                        println("\t $err")
                    }
                }
            }
        }
        println(solver.dynamicKb)
    }
}
