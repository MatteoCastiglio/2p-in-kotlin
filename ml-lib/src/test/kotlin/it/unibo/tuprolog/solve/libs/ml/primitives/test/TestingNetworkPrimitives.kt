package it.unibo.tuprolog.solve.libs.ml.primitives.test

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.dsl.theory.prolog
import it.unibo.tuprolog.solve.halt
import it.unibo.tuprolog.solve.hasSolutions
import it.unibo.tuprolog.solve.libs.ml.TestCSV.mockDataSet2
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.activationDomainError
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.instanceTypeError
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.integerTypeError
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.iris
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.lossDomainError
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.nonNegativeDomainError
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefDataset
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefDenseLayer
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefInputLayer
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefNetwork
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefOutputLayer
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefTypeError
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.runtimeError
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.tempPath
import it.unibo.tuprolog.solve.no
import it.unibo.tuprolog.solve.yes
import kotlin.collections.listOf as ktListOf

object TestingNetworkPrimitives {

    /**
     * input_layer Testing
     *
     * Contained requests:
     * ```prolog
     * ?- input_layer(atom,I). -> type_error(integer,atom)
     * ?- input_layer(-10, I). -> domain_error(not_less_than_zero,-10)
     * ?- input_layer([10],I). -> yes, I/objectRefInputLayer
     * ?- input_layer(5,objectRefInputLayer). -> no
     * ?- input_layer(X,objectRefInputLayer). -> yes, X/10
     * ?- input_layer(X,objectRefDenseLayer). -> instance_type_error(input_layer,objectRefDenseLayer)
     * ```
     */
    val InputLayerTesting by lazy {
        prolog {
            ktListOf(
                "input_layer"("atom", X).hasSolutions({ halt(integerTypeError) }),
                "input_layer"(-10, X).hasSolutions({ halt(nonNegativeDomainError) }),
                "input_layer"(10, Y).hasSolutions({ yes(Y to objRefInputLayer) }),
                "input_layer"(-10, objRefInputLayer).hasSolutions({ no() }),
                "input_layer"(X, objRefInputLayer).hasSolutions({ yes(X to 4) }),
                "input_layer"(X, objRefDenseLayer).hasSolutions({ halt(instanceTypeError) })
            )
        }
    }

    /**
     * dense_layer Testing
     *
     * Contained requests:
     * ```prolog
     * ?- dense_layer(objRefInputLayer, 10, activation(relu),D). -> type_error(integer,atom)
     * ?- dense_layer(objRefInputLayer, -10, activation(relu),D). -> domain_error(not_less_than_zero,-10)
     * ?- dense_layer(objRefInputLayer,10, not_an_activation ,D). -> domain_error(activation, not_an_activation)
     * ?- dense_layer(atom ,10, activation(relu) ,D). -> type_error(object_reference, atom)
     * ?- dense_layer(objRefInputLayer, 10, activation(relu) ,D). ->  yes, D/objectRefDenseLayer
     * ?- dense_layer(I, 5, activation(relu), objectRefDenseLayer). -> no
     * ?- dense_layer(I, X, Y, objectRefDenseLayer). -> yes, I/objectRefInputLayer X/10 Y/activation(relu)
     * ?- dense_layer(I, X, Y, objectRefInputLayer). -> instance_type_error(dense_layer,objectRefInputLayer)
     * ```
     */
    val DenseLayerTesting by lazy {
        prolog {
            ktListOf(
                "dense_layer"(
                    objRefInputLayer,
                    10,
                    "not_an_activation",
                    D
                ).hasSolutions({ halt(activationDomainError) }),
                "dense_layer"(
                    objRefInputLayer,
                    "atom",
                    "activation"("relu"),
                    D
                ).hasSolutions({ halt(integerTypeError) }),
                "dense_layer"(
                    objRefInputLayer,
                    -10,
                    "activation"("relu"),
                    D
                ).hasSolutions({ halt(nonNegativeDomainError) }),
                "dense_layer"("atom", 10, "activation"("relu"), D).hasSolutions({ halt(objRefTypeError) }),
                "dense_layer"(
                    objRefInputLayer,
                    10,
                    "activation"("relu"),
                    D
                ).hasSolutions({ yes(D to objRefDenseLayer) }),
                "dense_layer"(I, 5, "activation"("relu"), objRefDenseLayer).hasSolutions({ no() }),
                "dense_layer"(I, X, Y, objRefDenseLayer).hasSolutions({
                    yes(
                        I to objRefInputLayer,
                        X to 8,
                        Y to "activation"("relu")
                    )
                }),
                "dense_layer"(I, 10, "activation"("relu"), objRefInputLayer).hasSolutions({ halt(instanceTypeError) })
            )
        }
    }

    /**
     * output_layer Testing
     *
     * Contained requests:
     * ```prolog
     * ?- output_layer(objRefDenseLayer, 10, activation(relu),loss(mse), O). -> type_error(integer,atom)
     * ?- output_layer(objRefDenseLayer, -10, activation(relu),loss(mse) ,O). -> domain_error(not_less_than_zero,-10)
     * ?- output_layer(objRefDenseLayer,10, not_an_activation ,loss(mse),O). -> domain_error(activation, not_an_activation)
     * ?- output_layer(objRefDenseLayer ,10, not_an_activation,not_a_loss ,O). -> domain_error(loss, not_a_loss)
     * ?- output_layer(atom ,10, activation(relu) loss(mse) ,D). -> type_error(object_reference, atom)
     * ?- output_layer(objRefDenseLayer, 10, activation(relu),loss(mse) ,O). ->  yes, D/objectRefDenseLayer
     * ?- output_layer(D, 5, activation(relu),loss(mse), objectRefOutputLayer). -> no
     * ?- output_layer(D, X, Y, Z,objectRefOutputLayer). -> yes, I/objectRefInputLayer X/10 Y/activation(relu) Z/loss(mse)
     * ?- output_layer(D, X, Y, Z, objectRefInputLayer). ->  instancetype_error(output_layer,objectRefInputLayer)
     * ```
     */
    val OutputLayerTesting by lazy {
        prolog {
            ktListOf(

                "output_layer"(objRefDenseLayer, 10, "not_an_activation", "loss"("mse"), O).hasSolutions({
                    halt(activationDomainError)
                }),
                "output_layer"(objRefDenseLayer, 10, "activation"("relu"), "not_a_loss", O).hasSolutions({
                    halt(lossDomainError)
                }),
                "output_layer"(objRefDenseLayer, "atom", "activation"("relu"), "loss"("mse"), O).hasSolutions({
                    halt(integerTypeError)
                }),
                "output_layer"(objRefDenseLayer, -10, "activation"("relu"), "loss"("mse"), O).hasSolutions({
                    halt(nonNegativeDomainError)
                }),
                "output_layer"("atom", "atom", "activation"("relu"), "loss"("mse"), O).hasSolutions({
                    halt(objRefTypeError)
                }),
                "output_layer"(objRefDenseLayer, 10, "activation"("relu"), "loss"("mse"), O).hasSolutions({
                    yes(O to objRefOutputLayer)
                }),
                "output_layer"(D, 5, "activation"("relu"), "loss"("mse"), objRefOutputLayer).hasSolutions({ no() }),
                "output_layer"(D, X, Y, Z, objRefOutputLayer).hasSolutions({
                    yes(D to objRefDenseLayer, X to 3, Y to "activation"("softmax"), Z to "loss"("cross_entropy"))
                }),
                "output_layer"(D, X, Y, Z, objRefInputLayer).hasSolutions({ halt(instanceTypeError) }),
            )
        }
    }

    /**
     * build_network Testing
     *
     * Contained requests:
     * ```prolog
     * ?- build_network(atom, N ). -> type_error(object_reference,atom)
     * ```
     */
    val BuildNetworkTestingKO by lazy {
        prolog {
            ktListOf(
                "network"("atom", N).hasSolutions({ halt(objRefTypeError) }),
            )
        }
    }

    /**
     * build_network Testing
     *
     * Contained requests:
     * ```prolog
     * ?- input_layer([10],I), dense_layer(I, 10, activation(relu) ,D),
     *    output_layer(D, 10, activation(relu),loss(mse) ,O),build_network(O, N ) -> yes I/objectRefInputLayer D/objectRefDenseLayer O/objectRefOutputLayer N/objectRefNetwork
     * ?- build_network(O, objectRefNetwork) -> yes O/objectRefOutputLayer
     * ```
     */
    val BuildNetworkTestingOK by lazy {
        prolog {
            ktListOf(
                tupleOf(
                    "input_layer"(4, I),
                    "dense_layer"(I, 8, "activation"("relu"), D),
                    "output_layer"(D, 2, "activation"("relu"), "loss"("mse"), O),
                    "network"(O, N)
                ).hasSolutions({
                    yes(
                        I to objRefInputLayer,
                        D to objRefDenseLayer,
                        O to objRefOutputLayer,
                        N to objRefNetwork
                    )
                }),
                "network"(O, objRefNetwork).hasSolutions({ yes(O to objRefOutputLayer) }),
            )
        }
    }

    /**
     * predict_dataset Testing
     *
     * Contained requests:
     * ```prolog
     * :- load_dataset_from_csv(src/test/mockDataset2.csv, D), predict_dataset(D, objRefNetwork, prediction.csv , OUT) -> OUT to  objRefDataset * ```
     */
    val PredictDatasetTesting by lazy {
        prolog {
            ktListOf(
                tupleOf(
                    "load_dataset_from_csv"(mockDataSet2, D),
                    "predict_dataset"(D, objRefNetwork, tempPath, O)
                ).hasSolutions({ yes(D to objRefDataset, O to objRefDataset) }),
            )
        }
    }

    /**
     * classify Testing (only check yes or no and not actual substitution)
     *
     * Contained requests:
     * ```prolog
     * :-  classify(iris(petal_width(-0.04), petal_length(-1.33), sepal_width(-0.41), sepal_length(-0.50), objRefNetwork, [setosa, virginica, versicolor], OUT)) -> yes
     */
    val ClassifyTesting by lazy {
        prolog {
            ktListOf(
                "classify"(
                    iris(1.4, 3.4, 1.4, 12.3),
                    objRefNetwork,
                    listOf("setosa", "virginica", "versicolor"),
                    O
                ).hasSolutions({
                    yes(O to "setosa")
                })
            )
        }
    }

    /**
     * predict Testing (only check yes or no and not actual substitution)
     *
     * Contained requests:
     * ```prolog
     * :-  predict(iris(petal_width(-0.04), petal_length(-1.33), sepal_width(-0.41), sepal_length(-0.50), objRefNetwork, OUT)) -> yes
     */
    val PredictTesting by lazy {
        prolog {
            ktListOf(
                "predict"(iris(1.4, 3.4, 1.4, 12.3), objRefNetwork, O).hasSolutions({
                    yes(O to listOf(0.5, 0.5, 0.5))
                }),
            )
        }
    }

    /**
     * classify Testing
     *
     * Contained requests:
     * ```prolog
     * ?-   classify(iris( petal_length(..), petal_width(..), sepal_length(..), sepal_width(..), objRefNetwork,categories_list, Label) ->
     *          yes label/setosa
     * ?- classify(iris( petal_length(..), petal_width(..), sepal_length(..), X, objRefNetwork,categories_list, Label) -> ->
     *          no
     * ?- classify(iris( petal_length(..), petal_width(..), sepal_length(..), sepal_width(X), objRefNetwork,categories_list, Label) -> ->
     *          no
     * ?- classify(iris( petal_length(..), petal_width(..), sepal_length(..), "atom", X, objRefNetwork,categories_list, Label) ->
     *          halt runtimeError
     *
     */
    val ClassifyWithVarTesting by lazy {
        prolog {
            val categories = listOf("setosa", "versicolor", "virginica")
            kotlin.collections.listOf(
                "classify"(iris(4.9, 3.0, 1.4, 0.2), objRefNetwork, categories, L).hasSolutions({
                    yes(L to "setosa")
                }),
                "classify"(iris(4.9, 3.0, 1.4, X), objRefNetwork, categories, L).hasSolutions({
                    no()
                }),
                "classify"(iris(4.9, 3.0, 1.4, X), objRefNetwork, categories, L).hasSolutions({
                    no()
                }),
                "classify"(iris(4.9, 3.0, 1.4, Atom.of("atom")), objRefNetwork, categories, L).hasSolutions({
                    halt(runtimeError)
                })
            )
        }
    }

    /**
     * predict Testing
     *
     * Contained requests:
     * ```prolog
     * ?-  predict(iris( petal_length(..), petal_width(..), sepal_length(..), sepal_width(..), objRefNetwork, Output) ->
     *          yes label/setosa
     * ?- predict(iris( petal_length(..), petal_width(..), sepal_length(..), X, objRefNetwork, Output) -> ->
     *          no
     * ?- predict(iris( petal_length(..), petal_width(..), sepal_length(..), sepal_width(X), objRefNetwork, Output) -> ->
     *          no
     * ?- predict(iris( petal_length(..), petal_width(..), sepal_length(..), "atom", X, objRefNetwork, Output) ->
     *          halt runtimeError
     *
     */
    val PredictWithVarTesting by lazy {
        prolog {
            kotlin.collections.listOf(
                "predict"(iris(4.9, 3.0, 1.4, 0.2), objRefNetwork, O).hasSolutions({
                    yes(O to listOf(0.5, 0.5, 0.5))
                }),
                "predict"(iris(4.9, 3.0, 1.4, X), objRefNetwork, O).hasSolutions({
                    no()
                }),
                "predict"(iris(4.9, 3.0, 1.4, X), objRefNetwork, O).hasSolutions({
                    no()
                }),
                "predict"(iris(4.9, 3.0, 1.4, Atom.of("atom")), objRefNetwork, O).hasSolutions({
                    halt(runtimeError)
                })
            )
        }
    }
}
