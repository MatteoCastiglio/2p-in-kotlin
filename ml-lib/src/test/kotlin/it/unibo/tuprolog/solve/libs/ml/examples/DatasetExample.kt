package it.unibo.tuprolog.solve.libs.ml.examples

import it.unibo.tuprolog.dsl.theory.prolog
import it.unibo.tuprolog.solve.Solution
import it.unibo.tuprolog.solve.Solver
import it.unibo.tuprolog.solve.classicWithDefaultBuiltins
import it.unibo.tuprolog.solve.library.Libraries
import it.unibo.tuprolog.solve.libs.ml.MLLib
import it.unibo.tuprolog.solve.libs.ml.TestCSV
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.iris

fun main() {
    prolog {
        val solver = Solver.classicWithDefaultBuiltins(
            libraries = Libraries.of(
                MLLib
            ),
            staticKb = theoryOf(
                fact { iris(5.1, 3.0, 1.4, 0.2, "theory") },
                fact { iris(4.9, 3.0, 1.4, 0.2, "theory") },
                fact { iris(4.5, 3.0, 1.3, 0.2, "theory") },
                fact { iris(4.9, 2.4, 1.4, 0.2, "theory") },
                fact { "attribute"(0, "sepal_length", "type"("double")) },
                fact { "attribute"(1, "sepal_width", "type"("double")) },
                fact { "attribute"(2, "petal_length", "type"("double")) },
                fact { "attribute"(3, "petal_width", "type"("double")) },

                fact {
                    "attribute"(
                        4,
                        "class",
                        "type"("categorical"),
                        listOf("setosa", "versicolor", "virginica")
                    )
                },
            )
        )

        val query = tupleOf(
            "load_dataset_from_csv"(TestCSV.iris, D),
            "schema_from_theory"(S),
            "theory_from_dataset"("iris", S, D, X)
            // "theory_from_dataset"("iris", S, D, iris(5.1, 3.5, 1.4, 0.2, X))
        )

        solver.solve(query).forEach {
            when (it) {
                is Solution.No -> println("no.\n")
                is Solution.Yes -> {
                    println("yes: ${it.solvedQuery}")
                    for (assignment in it.substitution) {
                        println("\t${assignment.key} / ${assignment.value}")
                    }
                    println()
                }
                is Solution.Halt -> {
                    println("halt: ${it.exception.message}")
                    for (err in it.exception.prologStackTrace) {
                        println("\t $err")
                    }
                }
            }
        }
        println(solver.dynamicKb)
    }
}
