package it.unibo.tuprolog.solve.libs.ml.primitives.test

import it.unibo.tuprolog.solve.Solution
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import kotlin.test.assertEquals
import kotlin.test.assertTrue

/**
 *
 * Methods copied  from  it.unibo.solve.solve.test
 * On [Solution.Halt] check made only of exception type. (this is also said in the original definition but it doesn't seem to work like that)
 * For Object_Ref only class type is considered for easier test writing

 */

fun assertSolutionEqualsDL(expected: Solution, actual: Solution) {
    fun reportMsg(expected: Any, actual: Any, motivation: String = "") =
        "Expected: `$expected`\nActual\t: `$actual`" + if (motivation.isNotBlank()) " ($motivation)" else ""

    fun assertSameClass(expected: Solution, actual: Solution) =
        assertEquals(expected::class, actual::class, reportMsg(expected, actual))

    fun assertSameQuery(expected: Solution, actual: Solution) =
        assertEquals(expected.query, actual.query, reportMsg(expected, actual))

    fun assertEqualsSubstitution(expected: Solution, actual: Solution) {
        assertSameClass(expected, actual)
        assertSameQuery(expected, actual)
        assertEquals(expected.substitution.count(), actual.substitution.count(), reportMsg(expected, actual))
        assertEquals(expected.substitution.keys, actual.substitution.keys, reportMsg(expected, actual))

        expected.substitution.forEach { (varExpected, termExpected) ->
            actual.substitution[varExpected]!!.let { termActual ->
                if (termActual is ObjectRef && termExpected is ObjectRef) {
                    assertTrue(
                        reportMsg(
                            termExpected,
                            termActual
                        )
                    ) { termActual.`object`.javaClass == termExpected.`object`.javaClass }
                } else {
                    assertTrue(reportMsg(termExpected, termActual)) { termActual.structurallyEquals(termExpected) }
                }

                // if the substitution contain variables, compare only names, because instances will be different
                assertEquals(
                    termExpected.variables.map { it.name }.toList(),
                    termActual.variables.map { it.name }.toList(),
                    "Comparing variable names of expected `$expected` with `$actual`"
                )
            }
        }
    }

    when {
        expected is Solution.Halt -> {
            assertSameClass(expected, actual)
            assertSameQuery(expected, actual)
            assertEquals(expected.substitution, actual.substitution, reportMsg(expected, actual, "Wrong substitution"))
            assertTrue(reportMsg(expected, actual, "Solution is not Halt")) { actual is Solution.Halt }
            assertEquals(
                expected.exception::class,
                (actual as Solution.Halt).exception::class,
                reportMsg(expected, actual, "Wrong exception type")
            )
            /*   when (val expectedEx = expected.exception) {
            is PrologError -> {
                assertTrue(
                        reportMsg(
                                expected,
                                actual,
                                "Exception is not PrologError"
                        )
                ) { actual.exception is PrologError }
                val actualEx = actual.exception as PrologError
                assertTrue(reportMsg(expected, actual, "The error structs do not match")) {
                    expectedEx.errorStruct.equals(actualEx.errorStruct, false)
                }
                assertEquals(
                        expectedEx.message,
                        actualEx.message,
                        reportMsg(expected, actual, "Different messages")
                )
            }
        }*/
        }

        else -> assertEqualsSubstitution(expected, actual)
    }
}

fun assertSolutionEqualsSoft(expected: Solution, actual: Solution) {
    fun reportMsg(expected: Any, actual: Any, motivation: String = "") =
        "Expected: `$expected`\nActual\t: `$actual`" + if (motivation.isNotBlank()) " ($motivation)" else ""

    fun assertSameClass(expected: Solution, actual: Solution) =
        assertEquals(expected::class, actual::class, reportMsg(expected, actual))

    fun assertSameQuery(expected: Solution, actual: Solution) =
        assertEquals(expected.query, actual.query, reportMsg(expected, actual))

    fun assertEqualsSubstitution(expected: Solution, actual: Solution) {
        assertSameClass(expected, actual)
        assertSameQuery(expected, actual)
        assertEquals(expected.substitution.count(), actual.substitution.count(), reportMsg(expected, actual))
        assertEquals(expected.substitution.keys, actual.substitution.keys, reportMsg(expected, actual))
    }
    assertEqualsSubstitution(expected, actual)
}
