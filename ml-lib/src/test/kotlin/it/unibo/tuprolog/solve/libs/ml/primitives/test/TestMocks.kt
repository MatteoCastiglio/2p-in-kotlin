package it.unibo.tuprolog.solve.libs.ml.primitives.test

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.List
import it.unibo.tuprolog.core.Numeric
import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.dsl.theory.prolog
import it.unibo.tuprolog.solve.DummyInstances
import it.unibo.tuprolog.solve.exception.error.DomainError
import it.unibo.tuprolog.solve.exception.error.TypeError
import it.unibo.tuprolog.solve.libs.ml.Attribute
import it.unibo.tuprolog.solve.libs.ml.LogicActivation
import it.unibo.tuprolog.solve.libs.ml.LogicDataSet
import it.unibo.tuprolog.solve.libs.ml.LogicLoss
import it.unibo.tuprolog.solve.libs.ml.LogicNetwork
import it.unibo.tuprolog.solve.libs.ml.LogicTransformProcess
import it.unibo.tuprolog.solve.libs.ml.TestCSV
import it.unibo.tuprolog.solve.libs.ml.exception.DomainErrorML
import it.unibo.tuprolog.solve.libs.ml.exception.InstanceTypeError
import it.unibo.tuprolog.solve.libs.ml.exception.RuntimeError
import it.unibo.tuprolog.solve.libs.ml.impl.network.layers.LogicLayerFactory
import it.unibo.tuprolog.solve.libs.ml.impl.schema.LogicSchemaImpl
import it.unibo.tuprolog.solve.libs.ml.impl.schema.LogicTransformProcessImpl
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import java.io.File

object TestMocks {

    private val aContext = DummyInstances.executionContext

    /**errors**/
    val activationDomainError = DomainErrorML(context = aContext, expectedDomain = DomainErrorML.Expected.ACTIVATION, actualValue = Atom.of("not_an_activation"))
    val lossDomainError = DomainErrorML(context = aContext, expectedDomain = DomainErrorML.Expected.LOSS, actualValue = Atom.of("not_a_loss"))
    val integerTypeError = TypeError(context = aContext, expectedType = TypeError.Expected.INTEGER, culprit = Atom.of("atom"))
    val nonNegativeDomainError = DomainError(context = aContext, expectedDomain = DomainError.Expected.NOT_LESS_THAN_ZERO, actualValue = Numeric.of(-5))
    val objRefTypeError = TypeError(context = aContext, expectedType = TypeError.Expected.OBJECT_REFERENCE, culprit = Atom.of("atom"))
    val genericTypeError = TypeError(context = aContext, expectedType = TypeError.Expected.OBJECT_REFERENCE, culprit = Atom.of("atom"))
    val instanceTypeError = InstanceTypeError(context = aContext, expectedType = InstanceTypeError.Expected.LAYER, culprit = Atom.of("atom"))
    val runtimeError = RuntimeError(context = aContext, category = RuntimeError.Category.RUNTIME)

    /**iris schema**/
    private val attributeIris0 = Struct.of("attribute", Numeric.of(0), Atom.of("sepal_length"), Struct.of("type", Atom.of("double")))
    private val attributeIris1 = Struct.of("attribute", Numeric.of(1), Atom.of("sepal_width"), Struct.of("type", Atom.of("double")))
    private val attributeIris2 = Struct.of("attribute", Numeric.of(2), Atom.of("petal_length"), Struct.of("type", Atom.of("double")))
    private val attributeIris3 = Struct.of("attribute", Numeric.of(3), Atom.of("petal_width"), Struct.of("type", Atom.of("double")))
    private val attributeIris4 = Struct.of("attribute", Numeric.of(4), Atom.of("class"), Struct.of("type", Atom.of("categorical")), List.of(Atom.of("setosa"), Atom.of("versicolor"), Atom.of("virginica")))
    val attributes = listOf(attributeIris0, attributeIris1, attributeIris2, attributeIris3, attributeIris4)
    private val attributeIris4Train = Struct.of("attribute", Numeric.of(4), Atom.of("class[setosa]"), Struct.of("type", Atom.of("integer")))
    private val attributeIris5Train = Struct.of("attribute", Numeric.of(5), Atom.of("class[versicolor]"), Struct.of("type", Atom.of("integer")))
    private val attributeIris6Train = Struct.of("attribute", Numeric.of(6), Atom.of("class[virginica]"), Struct.of("type", Atom.of("integer")))
    val trainingAttributes = listOf(attributeIris0, attributeIris1, attributeIris2, attributeIris3, attributeIris4Train, attributeIris5Train, attributeIris6Train)

    /**medical_cost schema**/
    private val attributeMC0 = Struct.of("attribute", Numeric.of(0), Atom.of("age"), Struct.of("type", Atom.of("integer")))
    private val attributeMC1 = Struct.of("attribute", Numeric.of(1), Atom.of("sex"), Struct.of("type", Atom.of("categorical")), List.of(Atom.of("female"), Atom.of("male")))
    private val attributeMC2 = Struct.of("attribute", Numeric.of(2), Atom.of("bmi"), Struct.of("type", Atom.of("double")))
    private val attributeMC3 = Struct.of("attribute", Numeric.of(3), Atom.of("children"), Struct.of("type", Atom.of("integer")))
    private val attributeMC4 = Struct.of("attribute", Numeric.of(4), Atom.of("smoker"), Struct.of("type", Atom.of("categorical")), List.of(Atom.of("yes"), Atom.of("no")))
    private val attributeMC5 = Struct.of("attribute", Numeric.of(5), Atom.of("region"), Struct.of("type", Atom.of("categorical")), List.of(Atom.of("northeast"), Atom.of("southeast"), Atom.of("southwest"), Atom.of("northwest")))
    private val attributeMC6 = Struct.of("attribute", Numeric.of(6), Atom.of("cost"), Struct.of("type", Atom.of("double")))
    val attributesMC = listOf(attributeMC0, attributeMC1, attributeMC2, attributeMC3, attributeMC4, attributeMC5, attributeMC6)

    /**Object_Ref**/
    private val inputLayer = LogicLayerFactory.inputLayer(4L)
    private val denseLayer = LogicLayerFactory.denseLayer(8L, LogicActivation.RELU, inputLayer)
    private val outputLayer = LogicLayerFactory.outputLayer(3L, LogicActivation.SOFTMAX, LogicLoss.CROSS_ENTROPY, denseLayer)
    private val inputLayerMC = LogicLayerFactory.inputLayer(3L)
    private val denseLayerMC = LogicLayerFactory.denseLayer(10L, LogicActivation.RELU, inputLayerMC)
    private val outputLayerMC = LogicLayerFactory.outputLayer(1L, LogicActivation.RELU, LogicLoss.MSE, denseLayerMC)
    val objRefNetworkMC = ObjectRef.of(LogicNetwork.of(outputLayerMC))
    val schema = LogicSchemaImpl.of(Attribute.getAttributes(attributes))
    private val transformProcess = LogicTransformProcessImpl.fromSchema(schema)
    val objRefInputLayer = ObjectRef.of(inputLayer)
    val objRefDenseLayer = ObjectRef.of(denseLayer)
    val objRefOutputLayer = ObjectRef.of(outputLayer)
    val objRefNetwork = ObjectRef.of(LogicNetwork.of(outputLayer))
    val objRefDataset = ObjectRef.of(LogicDataSet.fromCsv(TestCSV.iris))
    val objRefSchema = ObjectRef.of(schema)
    val objRefTransformProcess = ObjectRef.of(transformProcess)
    private val normalizedSchema = LogicTransformProcess.normalize(schema, listOf("petal_width", "petal_length", "sepal_width", "sepal_length"))
    private val encodedSchema = LogicTransformProcess.oneHotEncoding(normalizedSchema, listOf("class"))
    val objRefTrainingSchema = ObjectRef.of(encodedSchema)
    private val dropClassSchema = LogicTransformProcess.deleteColumns(schema, listOf("class"))
    val objRefDropClassTp = ObjectRef.of(LogicTransformProcess.fromSchema(dropClassSchema))
    val objRefTrainingTP = ObjectRef.of(LogicTransformProcess.fromSchema(encodedSchema))
    val objRefNoFitTP = ObjectRef.of(LogicTransformProcess.fromSchema(normalizedSchema))

    /** files **/
    val tempPath = File.createTempFile("randomFile", ".csv").absolutePath

    /**iris Utils**/
    fun iris(sl: Double, sw: Double, pl: Double, pw: Double, klass: String): Struct =
        prolog {
            "iris"("sepal_length"(sl), "sepal_width"(sw), "petal_length"(pl), "petal_width"(pw), "class"(klass))
        }

    fun iris(sl: Double, sw: Double, pl: Double, pw: Double): Struct =
        prolog {
            "iris"("sepal_length"(sl), "sepal_width"(sw), "petal_length"(pl), "petal_width"(pw))
        }

    fun iris(sl: Double, sw: Double, pl: Double, pw: Double, vararg terms: Term): Struct =
        prolog {
            "iris"("sepal_length"(sl), "sepal_width"(sw), "petal_length"(pl), "petal_width"(pw), *terms)
        }

    fun iris(sl: Double, sw: Double, pl: Double, vararg terms: Term): Struct =
        prolog {
            "iris"("sepal_length"(sl), "sepal_width"(sw), "petal_length"(pl), *terms)
        }

    /** medical cost Utils **/
    fun mc(age: Int, sex: String, bmi: Double, children: Int, smoker: String, region: String, cost: Double): Struct =
        prolog {
            "medical_cost"("age"(age), "sex"(sex), "bmi"(bmi), "children"(children), "smoker"(smoker), "region"(region), "cost"(cost))
        }
    fun mc(age: Int, sex: String, bmi: Double, children: Int, smoker: String, region: String, cost: Term): Struct =
        prolog {
            "medical_cost"("age"(age), "sex"(sex), "bmi"(bmi), "children"(children), "smoker"(smoker), "region"(region), cost)
        }
}
