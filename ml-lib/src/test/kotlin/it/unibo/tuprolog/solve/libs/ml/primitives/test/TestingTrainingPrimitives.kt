package it.unibo.tuprolog.solve.libs.ml.primitives.test

import it.unibo.tuprolog.dsl.theory.prolog
import it.unibo.tuprolog.solve.hasSolutions
import it.unibo.tuprolog.solve.libs.ml.TestCSV
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.attributesMC
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefDataset
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefNetwork
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefNetworkMC
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefSchema
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefTrainingSchema
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefTrainingTP
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.tempPath
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.trainingAttributes
import it.unibo.tuprolog.solve.yes

object TestingTrainingPrimitives {

    /**
     * train Testing
     *
     * Contained requests:
     * ```prolog
     * fit(objRefDataset, objRefTrainingTP),  transform_dataset(objRefDataset, objRefTrainingTP, traindata.csv, D),
     *      train( objRefNetwork, D, 4,6,10) -> D/objRefDataset
     */
    val TrainTesting by lazy {
        prolog {
            kotlin.collections.listOf(
                "schema_attributes"(L, objRefTrainingSchema).hasSolutions({ yes(L to trainingAttributes) }),
                tupleOf(
                    "fit"(objRefTrainingTP, objRefDataset),
                    "transform_dataset"(objRefDataset, objRefTrainingTP, tempPath, D),
                    "train"(objRefNetwork, D, 4, 6, 10)
                ).hasSolutions({ yes(D to objRefDataset) })
            )
        }
    }

    /**
     * train4 Testing
     *
     * Contained requests:
     * ```prolog
     *  :- schema_attributes(attributesMC, S),  drop_columns(S, [age, sex, smoker, region] X),
     *          transform_process(X, T), load_dataset_from_csv(TestCSV.medicalCost, D),
     *          transform_dataset(D, objRefTrainingTP, tempPath, 2), train(objRefNetworkMC, O, 6, 10)
     */
    val Train4Testing by lazy {
        prolog {
            kotlin.collections.listOf(
                tupleOf(
                    "schema_attributes"(attributesMC, S),
                    "delete_columns"(S, listOf("sex", "smoker", "region"), X),
                    "transform_process"(X, T),
                    "load_dataset_from_csv"(TestCSV.medicalCost, D),
                    "transform_dataset"(D, T, tempPath, O),
                    "train"(objRefNetworkMC, O, 2, 10)
                ).hasSolutions({
                    yes(
                        S to objRefSchema,
                        X to objRefSchema,
                        T to objRefTrainingTP,
                        D to objRefDataset,
                        O to objRefDataset
                    )
                })
            )
        }
    }

    /**
     * accuracy Testing (check only on yes/no/halt response)
     *
     * Contained requests:
     * ```prolog
     * fit(objRefDataset, objRefTrainingTP),  transform_dataset(objRefDataset, objRefTrainingTP, traindata.csv, D),
     *      accuracy( objRefNetworkMC, D, 4,6, X) -> D/objRefDataset and X/0.5
     */
    val AccuracyTesting by lazy {
        prolog {
            kotlin.collections.listOf(
                tupleOf(
                    "fit"(objRefTrainingTP, objRefDataset),
                    "transform_dataset"(objRefDataset, objRefTrainingTP, tempPath, D),
                    "accuracy"(objRefNetwork, D, 4, 6, X)
                ).hasSolutions({ yes(D to objRefDataset, X to .50) })
            )
        }
    }

    /**
     * mse Testing
     *
     * Contained requests:
     * ```prolog
     *  :- schema_attributes(attributesMC, S),  drop_columns(S, [age, sex, smoker, region] X),
     *          transform_process(X, T), load_dataset_from_csv(TestCSV.medicalCost, D),
     *          transform_dataset(D, objRefTrainingTP, tempPath, 2), mse(objRefNetwork, O, 6, 10)
     */
    val MSETesting by lazy {
        prolog {
            kotlin.collections.listOf(
                tupleOf(
                    "schema_attributes"(attributesMC, S),
                    "delete_columns"(S, listOf("sex", "smoker", "region"), X),
                    "transform_process"(X, T),
                    "load_dataset_from_csv"(TestCSV.medicalCost, D),
                    "transform_dataset"(D, T, tempPath, O),
                    "mse"(objRefNetworkMC, O, 2, M)
                ).hasSolutions({
                    yes(
                        S to objRefSchema,
                        X to objRefSchema,
                        T to objRefTrainingTP,
                        D to objRefDataset,
                        O to objRefDataset,
                        M to 0.5
                    )
                })
            )
        }
    }
}
