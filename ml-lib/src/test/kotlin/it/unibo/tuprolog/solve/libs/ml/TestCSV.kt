package it.unibo.tuprolog.solve.libs.ml

object TestCSV {
    val iris: String = TestCSV::class.java.getResource("iris.csv").file
    val medicalCost: String = TestCSV::class.java.getResource("medical_cost.csv").file
    val mockDataSet: String = TestCSV::class.java.getResource("mockDataset.csv").file
    val mockDataSet2: String = TestCSV::class.java.getResource("mockDataset2.csv").file
}
