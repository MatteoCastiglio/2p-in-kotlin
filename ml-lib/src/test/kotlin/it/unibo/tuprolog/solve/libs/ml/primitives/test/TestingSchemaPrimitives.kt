package it.unibo.tuprolog.solve.libs.ml.primitives.test

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.dsl.theory.prolog
import it.unibo.tuprolog.solve.halt
import it.unibo.tuprolog.solve.hasSolutions
import it.unibo.tuprolog.solve.libs.ml.TestCSV
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.attributes
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.genericTypeError
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.iris
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefDataset
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefDropClassTp
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefNetwork
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefSchema
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefTransformProcess
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.runtimeError
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.tempPath
import it.unibo.tuprolog.solve.no
import it.unibo.tuprolog.solve.yes

object TestingSchemaPrimitives {

    /**
     * schema_from_theory Testing
     *
     * Contained requests:
     * ```prolog
     * ?- schema_from_theory(S) -> yes S/objRefSchema with valid schema in thoery, look TestDlPrimitivesImpl
     * ```
     */
    val SchemaByTheoryTestingOk by lazy {
        prolog {
            kotlin.collections.listOf(
                "schema_from_theory"(S).hasSolutions({ yes(S to objRefSchema) }),
            )
        }
    }

    /**
     * schema_from_theory Testing
     *
     * Contained requests:
     * ```prolog
     * ?- schema_from_theory(S) -> error with invalid schema in thoery, look TestDlPrimitivesImpl
     * ```
     */
    val SchemaByTheoryTestingKO by lazy {
        prolog {
            kotlin.collections.listOf(
                "schema_from_theory"(S).hasSolutions({ halt(runtimeError) }),
            )
        }
    }

    /**
     * schema_attributes Testing
     *
     * Contained requests:
     * ```prolog
     * ?- schema_attributes(atom,S) -> halt type_error(list, "atom")
     * ?- schema_attributes(L ,"atom") -> halt type_error(object_ref, "atom")
     * * ?- schema_attributes(L ,schemaObjectRed) -> yes
     * ```
     */
    val AttributesSchemaTesting by lazy {
        prolog {
            kotlin.collections.listOf(
                "schema_attributes"("atom", S).hasSolutions({ halt(genericTypeError) }),
                "schema_attributes"(L, "atom").hasSolutions({ halt(genericTypeError) }),
                "schema_attributes"(L, objRefSchema).hasSolutions({ yes(L to attributes) }),
                "schema_attributes"(attributes, objRefSchema).hasSolutions({ yes() }),
                "schema_attributes"(attributes, S).hasSolutions({ yes(S to objRefSchema) }),
            )
        }
    }

    /**
     * schema transformation Testing
     *
     * Contained requests:
     * ```prolog
     * ?- normalize(atom,atom,atom) -> halt type_error(var, "atom")
     * ?- normalize(S1,atom,S2) -> halt instatation_error
     * ?- normalize(objRefSchema,S1,S2) -> halt instatation_error
     * ?- normalize(atom,atom,S2) -> halt type_error(object_ref,atom)
     * ?- normalize(objRefSchema,atom,S2) -> halt type_error(list,atom)
     * ? -normalize(objRefSchema,[petal_length,petal_width],S2) -> yes{S2 to objRefSchema}
     * ? -one_hot_encoding(objRefSchema,[class],S2) -> yes{S2 to objRefSchema}
     * ? -delete_columns(objRefSchema,[class],S2) -> yes{S2 to objRefSchema}
     */
    val SchemaTrasformationTesting by lazy {
        prolog {
            kotlin.collections.listOf(
                "normalize"("atom", "atom", "atom").hasSolutions({ halt(genericTypeError) }),
                "normalize"("atom", "atom", S).hasSolutions({ halt(genericTypeError) }),
                "normalize"(objRefSchema, "atom", S).hasSolutions({ halt(genericTypeError) }),
                "normalize"(objRefSchema, listOf("petal_length", "petal_width"), S).hasSolutions({ yes(S to objRefSchema) }),
                "one_hot_encoding"(objRefSchema, listOf("class"), S).hasSolutions({ yes(S to objRefSchema) }),
                "delete_columns"(objRefSchema, listOf("class"), S).hasSolutions({ yes(S to objRefSchema) }),
                "delete_columns"(objRefSchema, listOf("ffff"), S).hasSolutions({ halt(runtimeError) }),
                "one_hot_encoding"(objRefSchema, listOf("petal_length"), S).hasSolutions({ halt(runtimeError) })
            )
        }
    }

    /**
     * transform_process Testing
     *
     * Contained requests:
     * ```prolog
     * ?- transform_process(objRefSchema, T) -> T/objRefTransformProcess
     * ?- transform_process(S, objRefTransformProcess) -> S/objRefSchema
     */

    val TransformProcessTesting by lazy {
        prolog {
            kotlin.collections.listOf(
                "transform_process"(objRefSchema, T).hasSolutions({ yes(T to objRefTransformProcess) }),
                "transform_process"(S, objRefTransformProcess).hasSolutions({ yes(S to objRefSchema) })
            )
        }
    }

    /**
     * transform Testing
     *
     * Contained requests:
     * ```prolog
     * ?- normalize(objRefSchema, [sepal_length, sepal_width, petal_length, petal_width], S1), one_hot_encoding(S1,[class],S3), transform_process,T1),
     *          transform(iris( sepal_length(..), sepal_width(..), petal_length(..), petal_width(..), class(..), T1, OUT) ->
     *          yes OUT/ iris( sepal_length(..), sepal_width(..), petal_length(..), petal_width(..), class[setosa](..), class[versicolor] , class[virginica])
     * ?- normalize(objRefSchema,[sepal_length, sepal_width, petal_length, petal_width], S1), delete(S1,[class],S3), transform_process,T1),
     *          transform(iris( sepal_length(..), sepal_width(..), petal_length(..), petal_width(..), _ , T1, OUT) ->
     *          yes OUT/ iris( sepal_length(..), sepal_width(..), petal_length(..), petal_width(..))

     */
    val TransformTesting by lazy {
        prolog {
            kotlin.collections.listOf(
                "transform"("aaa", objRefDropClassTp, 0).hasSolutions({
                    halt(runtimeError)
                }),
                tupleOf(
                    "normalize"(objRefSchema, listOf("petal_length", "petal_width", "sepal_length", "sepal_width"), N),
                    "delete_columns"(N, listOf("class"), C),
                    "transform_process"(C, T),
                    "one_hot_encoding"(N, listOf("class"), O),
                    "transform_process"(O, "T2"),
                    "load_dataset_from_csv"(TestCSV.iris, D),
                    "fit"(T, D),
                    "fit"("T2", D),
                    "transform_dataset"(D, T, tempPath, "D2"),
                    "transform"(iris(4.9, 3.0, 1.4, 0.2, "setosa"), T, X),
                    "transform"(iris(4.9, 3.0, 1.4, 0.2, "setosa"), "T2", "X2")
                ).hasSolutions({
                    yes(
                        N to objRefSchema,
                        C to objRefSchema,
                        T to objRefTransformProcess,
                        O to objRefSchema,
                        "T2" to objRefTransformProcess,
                        D to objRefDataset,
                        "D2" to objRefDataset,
                        X to iris(-1.1392004834649512, -0.12454037930145648, -1.3367940202882502, -1.3085928194379572),
                        "X2" to iris(
                            -1.1392004834649512,
                            -0.12454037930145648,
                            -1.3367940202882502,
                            -1.3085928194379572,
                            "class[setosa]"(1),
                            "class[versicolor]"(0),
                            "class[virginica]"(0)
                        )
                    )
                })
            )
        }
    }

    /**
     * transform Testing
     *
     * Contained requests:
     * ```prolog
     * ?-   transform(iris( sepal_length(..), sepal_width(..), petal_length(..), petal_width(..), class(..), objRefDropClassTp, OUT) ->
     *          yes OUT/ iris( sepal_length(..), sepal_width(..), petal_length(..), petal_width(..))
     * ?- transform(iris( sepal_length(..), sepal_width(..), petal_length(..), petal_width(..), X, objRefDropClassTp, OUT) ->
     *          yes OUT/ iris( sepal_length(..), sepal_width(..), petal_length(..), petal_width(..))
     * ?- transform(iris( sepal_length(..), sepal_width(..), petal_length(..), petal_width(..), class(X), objRefDropClassTp, OUT) ->
     *          yes OUT/ iris( sepal_length(..), sepal_width(..), petal_length(..), petal_width(..))
     * ?- transform(iris(sepal_length(..), sepal_width(..), petal_length(..) , "atom", X, objRefDropClassTp, OUT) ->
     *          halt runtimeError
     * ?- transform(iris( sepal_length(..), sepal_width(..), petal_length(..), aaa(...), X, objRefDropClassTp, OUT) ->
     *          halt runtimeError
     * ?- transform(iris( sepal_length(..), sepal_width(..), petal_length(..)Y , X, objRefDropClassTp, OUT) ->
     *          no
     * ?- transform(iris( sepal_length(..), sepal_width(..), petal_length(..), petal_width(Y), X, objRefDropClassTp, OUT) ->
     *          no
     */
    val TransformWithVarTesting by lazy {
        prolog {
            kotlin.collections.listOf(
                "transform"(iris(4.9, 3.0, 1.4, 0.2, "setosa"), objRefDropClassTp, O).hasSolutions({
                    yes(O to iris(4.9, 3.0, 1.4, 0.2))
                }),
                "transform"(iris(4.9, 3.0, 1.4, 0.2, X), objRefDropClassTp, O).hasSolutions({
                    yes(O to iris(4.9, 3.0, 1.4, 0.2))
                }),
                "transform"(iris(4.9, 3.0, 1.4, 0.2, "class"(X)), objRefDropClassTp, O).hasSolutions({
                    yes(O to iris(4.9, 3.0, 1.4, 0.2))
                }),
                "transform"(iris(4.9, 3.0, 1.4, 0.2, "class"(X)), objRefDropClassTp, O).hasSolutions({
                    yes(O to iris(4.9, 3.0, 1.4, 0.2))
                }),
                "transform"(iris(4.9, 3.0, 1.4, Atom.of("atom"), "class"(X)), objRefDropClassTp, O).hasSolutions({
                    halt(runtimeError)
                }),
                "transform"(iris(4.9, 3.0, 1.4, "aaa"(0.2), "class"(X)), objRefDropClassTp, O).hasSolutions({
                    halt(runtimeError)
                }),
                "transform"(iris(4.9, 3.0, 1.4, Y, "class"(X)), objRefDropClassTp, O).hasSolutions({
                    no()
                }),
                "transform"(iris(4.9, 3.0, 1.4, "sepal_width"(Y), "class"(X)), objRefDropClassTp, O).hasSolutions({
                    no()
                })
            )
        }
    }

    /**
     * theory_from_dataset Testing
     *
     * Contained requests:
     * ```prolog
     *  ?- load_dataset_from_csv(src/test/mockDataset.csv, D), theory_from_dataset(iris, objRefSchema, D, R)) ->
     *            yes { R/iris(sepal_length(5.1), sepal_width(3.5), petal_length(1.4), petal_width(.2), class(setosa)) } and
     *            yes { R/iris(sepal_length(4.9), sepal_width(3), petal_length(1.4), petal_width(.2), class(setosa)) }
     */
    val TheoryFromDatasetTesting by lazy {
        prolog {
            kotlin.collections.listOf(
                tupleOf(
                    "load_dataset_from_csv"(TestCSV.mockDataSet, D),
                    "theory_from_dataset"("iris", objRefSchema, D, R)
                ).hasSolutions(
                    {
                        yes(
                            D to objRefDataset,
                            R to iris(5.1, 3.5, 1.4, 0.2, "setosa")
                        )
                    },
                    {
                        yes(
                            D to objRefDataset,
                            R to iris(4.9, 3.0, 1.4, 0.2, "setosa")
                        )
                    }
                )
            )
        }
    }

    /**
     * enhance Testing
     *
     * Contained requests:
     * ```prolog
     *  ?- enhance(iris, objRefTransformProcess, objRefNetwork, class) -> yes
     */
    val EnhanceTesting by lazy {
        prolog {
            kotlin.collections.listOf(
                "enhance"("iris", objRefTransformProcess, objRefNetwork, "class").hasSolutions({ yes() })
            )
        }
    }
}
