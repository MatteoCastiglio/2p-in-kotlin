package it.unibo.tuprolog.solve.libs.ml.primitives.test

import it.unibo.tuprolog.dsl.theory.prolog
import it.unibo.tuprolog.solve.halt
import it.unibo.tuprolog.solve.hasSolutions
import it.unibo.tuprolog.solve.libs.ml.TestCSV
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.iris
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefDataset
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefNoFitTP
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.objRefSchema
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.runtimeError
import it.unibo.tuprolog.solve.yes

object TestingDataSetPrimitives {

    /**
     * load_dataset_from_csv Testing
     *
     * Contained requests:
     * ```prolog
     * ?- load_dataset_from_csv(iris,D) -> yes D/ objRefDataset
     * ?- load_dataset_from_csv(thereisnofilewiththisname.csv,D) -> halt runtimeError
     * ```
     */
    val LoadDataSetFromCSVTesting by lazy {
        prolog {
            kotlin.collections.listOf(
                "load_dataset_from_csv"(TestCSV.mockDataSet, D).hasSolutions({ yes(D to objRefDataset) }),
                "load_dataset_from_csv"("thereisnofilewiththisname.csv", D).hasSolutions({ halt(runtimeError) })
            )
        }
    }

    /**
     * dataset_from_theory Testing
     *
     * Contained requests:
     * ```prolog
     * :-iris(petal_width(5.1), petal_length(3.5), sepal_width(1.4), sepal_length(.2), class(setosa))
     * :-iris(petal_width(4.9), petal_length"(3), sepal_width(1.4), sepal_length(.2), class(setosa))
     * ?- load_dataset_from_theory(iris, D), theory_from_dataset(iris, objRefSchema, D, R)) ->
     *            yes { R/iris(petal_width(5.1), petal_length(3.5), sepal_width(1.4), sepal_length(.2), class(setosa)) } and
     *            yes { R/iris(petal_width(4.9), petal_length"(3), sepal_width(1.4), sepal_length(.2), class(setosa)) }
     */
    val LoadDataSetFromTheoryTesting by lazy {
        prolog {
            val query = "load_dataset_from_theory"("iris", D) and "theory_from_dataset"("iris", objRefSchema, D, R)
            // "iris"("petal_width"(5.1), "petal_length"(3.5), "sepal_width"(1.4), "sepal_length"(.2), "class"("setosa"))
            val sol1 = iris(5.1, 3.5, 1.4, 0.2, "setosa")
            // "iris"("petal_width"(4.9), "petal_length"(3), "sepal_width"(1.4), "sepal_length"(.2), "class"("setosa"))
            val sol2 = iris(4.9, 3.0, 1.4, 0.2, "setosa")
            kotlin.collections.listOf(
                query.hasSolutions(
                    { yes(D to objRefDataset, R to sol1) },
                    { yes(D to objRefDataset, R to sol2) }
                )
            )
        }
    }

    /**
     * random_split Testing
     *
     * Contained requests:
     * ```prolog
     * ?- random_split(objRefDataset, 0.7, D1, D2) ->  yes D1/objRefDataset,  D2/objRefDataset )
     */
    val RandomSplitTesting by lazy {
        prolog {
            kotlin.collections.listOf(
                "random_split"(objRefDataset, 0.7, "D1", "D2").hasSolutions(
                    { yes("D1" to objRefDataset, "D2" to objRefDataset) }
                ),
            )
        }
    }

    /**
     * transform_no_fit_error Testing
     *       *  Contained requests:
     * ```prolog
     * ?- transform_dataset(objRefDataset, objRefNoFitTP, error, D) -> runtime_error()
     */
    val TransformNoFitErrorTesting by lazy {
        prolog {
            kotlin.collections.listOf(
                "transform_dataset"(objRefDataset, objRefNoFitTP, "error", D).hasSolutions({ halt(runtimeError) }),
            )
        }
    }
}
