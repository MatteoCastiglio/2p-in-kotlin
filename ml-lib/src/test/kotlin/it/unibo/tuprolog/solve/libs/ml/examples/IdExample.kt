package it.unibo.tuprolog.solve.libs.ml.examples

import it.unibo.tuprolog.dsl.theory.prolog
import it.unibo.tuprolog.solve.Solution
import it.unibo.tuprolog.solve.Solver
import it.unibo.tuprolog.solve.classicWithDefaultBuiltins
import it.unibo.tuprolog.solve.library.Libraries
import it.unibo.tuprolog.solve.libs.ml.MLLib
import it.unibo.tuprolog.solve.libs.ml.TestCSV

fun main() {
    prolog {
        val solver = Solver.classicWithDefaultBuiltins(
            libraries = Libraries.of(
                MLLib
            ),
            staticKb = theoryOf(
                fact { "student"("id"(1), "name"("mario"), "surname"("rossi"), "target"(1.0)) },
                fact { "student"("id"(2), "name"("matteo"), "surname"("casti"), "target"(4.7)) },
                fact { "attribute"(0, "id", "type"("index")) },
                fact { "attribute"(1, "name", "type"("string")) },
                fact { "attribute"(2, "surname", "type"("string")) },
                fact { "attribute"(3, "target", "type"("double")) },
                rule {
                    "build_tp"(A) impliedBy
                        tupleOf(
                            "schema_from_theory"(I),
                            "delete_columns"(I, listOf("name", "surname"), S),
                            "transform_process"(S, T),
                            "assert"("registered"(A, T))

                        )
                },
                rule {
                    "build_network"(A) impliedBy
                        tupleOf(
                            "input_layer"(1, I),
                            "output_layer"(I, 1, "activation"("relu"), "loss"("mse"), O),
                            "network"(O, N),
                            "assert"("registered"(A, N))
                        )
                },
            )
        )

        val query = tupleOf(
            "load_dataset_from_csv"(TestCSV.iris, D),
            "schema_from_theory"(S),
            "build_tp"("tp"),
            "build_network"("network"),
            "registered"("tp", T),
            "registered"("network", N),
            "enhance"("student", T, N, "target"),
            "name"(1, X),
            "surname"(1, Y),
            "target"(1, W)

        )

        solver.solve(query).forEach {
            when (it) {
                is Solution.No -> println("no.\n")
                is Solution.Yes -> {
                    println("yes: ${it.solvedQuery}")
                    for (assignment in it.substitution) {
                        println("\t${assignment.key} / ${assignment.value}")
                    }
                    println()
                }
                is Solution.Halt -> {
                    println("halt: ${it.exception.message}")
                    for (err in it.exception.prologStackTrace) {
                        println("\t $err")
                    }
                }
            }
        }
        println(solver.dynamicKb)
    }
}
