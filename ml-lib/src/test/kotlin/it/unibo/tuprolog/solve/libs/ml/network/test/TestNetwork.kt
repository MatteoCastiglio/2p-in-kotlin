package it.unibo.tuprolog.solve.libs.ml.network.test

import it.unibo.tuprolog.core.Numeric
import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.solve.libs.ml.LogicActivation
import it.unibo.tuprolog.solve.libs.ml.LogicLoss
import it.unibo.tuprolog.solve.libs.ml.LogicNetwork
import it.unibo.tuprolog.solve.libs.ml.impl.network.layers.LogicLayerFactory
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TestNetwork {

    private val inputLayer = LogicLayerFactory.inputLayer(10L)
    private val denseLayer = LogicLayerFactory.denseLayer(10L, LogicActivation.SIGMOID, inputLayer)
    private val outputLayer = LogicLayerFactory.outputLayer(10L, LogicActivation.TANH, LogicLoss.MSE, denseLayer)
    private val network = LogicNetwork.of(outputLayer)

    @Test
    fun testLayerFactory() {
        assertTrue(outputLayer.size == 10L)
        assertEquals(outputLayer.activation, LogicActivation.TANH)
        assertEquals(outputLayer.previousLayer, denseLayer)
        assertTrue(denseLayer.size == 10L)
        assertEquals(denseLayer.activation, LogicActivation.SIGMOID)
        assertTrue(inputLayer.size == 10L)
    }

    @Test
    fun testNetworkFactory() {
        assertEquals(network.outputLayer, outputLayer)
        print(network.summary())
    }

    @Test
    fun testPredict() {
        val input = LogicLayerFactory.inputLayer(3)
        val output = LogicLayerFactory.outputLayer(1, LogicActivation.RELU, LogicLoss.MSE, input)
        val network = LogicNetwork.of(output)
        val res = network.predict(
            Struct.of(
                "vector",
                Struct.of("arg", Numeric.of(1)),
                Struct.of("arg", Numeric.of(1)),
                Struct.of(
                    "arg",
                    Numeric.of(1)
                )
            )
        )
        println(res)
        assert(res.size == 1)
    }
}
