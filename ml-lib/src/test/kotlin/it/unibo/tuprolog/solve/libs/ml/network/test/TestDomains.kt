package it.unibo.tuprolog.solve.libs.ml.network.test

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.solve.libs.ml.LogicActivation
import it.unibo.tuprolog.solve.libs.ml.LogicLoss
import org.junit.Test
import kotlin.test.assertEquals

class TestDomains {
    @Test
    fun testLoss() {
        assertEquals(LogicLoss.fromStruct(Struct.of("loss", listOf(Atom.of("mse")))), LogicLoss.MSE)
    }

    @Test
    fun testActivation() {
        assertEquals(LogicActivation.fromStruct(Struct.of("activation", listOf(Atom.of("relu")))), LogicActivation.RELU)
    }
}
