package it.unibo.tuprolog.solve.libs.ml.dataset.test

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.Numeric
import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.solve.libs.ml.Attribute
import it.unibo.tuprolog.solve.libs.ml.LogicActivation
import it.unibo.tuprolog.solve.libs.ml.LogicLoss
import it.unibo.tuprolog.solve.libs.ml.LogicNetwork
import it.unibo.tuprolog.solve.libs.ml.TestCSV
import it.unibo.tuprolog.solve.libs.ml.impl.dataset.LogicDataSetImpl
import it.unibo.tuprolog.solve.libs.ml.impl.network.layers.LogicLayerFactory
import it.unibo.tuprolog.solve.libs.ml.impl.schema.LogicTransformProcessImpl
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.schema
import org.junit.Test
import java.io.File
import kotlin.test.assertFails

class TestDataSet {

    @Test
    fun createLogicSchemaKO() {
        val attribute0 = Struct.of("attribute", Numeric.of(0), Atom.of("name"), Struct.of("type", Atom.of("index")))
        val attribute1 = Struct.of("attribute", Numeric.of(5), Atom.of("name"), Struct.of("type", Atom.of("index")))
        val attributes = listOf(attribute0, attribute1)
        assertFails { Attribute.getAttributes(attributes) }
        val attribute2 = Struct.of("attribute", Numeric.of(0), Atom.of("name"), Struct.of("type", Atom.of("ddd")))
        assertFails { Attribute.getAttributes(listOf(attribute2)) }
    }

    @Test
    fun train_evaluate() {
        val schema = schema
        val normalizedSchema = LogicTransformProcessImpl.normalize(schema, listOf("petal_length", "petal_width", "sepal_length", "sepal_width"))
        val encodedSchema = LogicTransformProcessImpl.oneHotEncoding(normalizedSchema, listOf("class"))
        val tp = LogicTransformProcessImpl.fromSchema(encodedSchema)
        val dataset = LogicDataSetImpl.fromCsv(TestCSV.iris)
        tp.fit(dataset)
        val tempPath = File.createTempFile("randomFile", ".csv").absolutePath
        val dataOut = tp.transformDataset(dataset, tempPath)
        val input = LogicLayerFactory.inputLayer(4)
        val hidden = LogicLayerFactory.denseLayer(20, LogicActivation.RELU, input)
        val output = LogicLayerFactory.outputLayer(3, LogicActivation.SOFTMAX, LogicLoss.CROSS_ENTROPY, hidden)
        val network = LogicNetwork.of(output)
        val accPreTrain = network.accuracy(dataOut, 4, 6)
        println(accPreTrain)
        network.train(dataOut, 4, 6, 50)
        val accPostTrain = network.accuracy(dataOut, 4, 6)
        println(accPostTrain)
        assert(accPostTrain > accPreTrain)
    }
}
