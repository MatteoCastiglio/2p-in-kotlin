package it.unibo.tuprolog.solve.libs.ml.examples

import it.unibo.tuprolog.dsl.theory.prolog
import it.unibo.tuprolog.solve.Solution
import it.unibo.tuprolog.solve.Solver
import it.unibo.tuprolog.solve.classicWithDefaultBuiltins
import it.unibo.tuprolog.solve.library.Libraries
import it.unibo.tuprolog.solve.libs.ml.MLLib
import it.unibo.tuprolog.solve.libs.ml.TestCSV
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.iris
import it.unibo.tuprolog.solve.libs.ml.primitives.test.TestMocks.tempPath

fun main() {
    prolog {
        val solver = Solver.classicWithDefaultBuiltins(
            libraries = Libraries.of(
                MLLib
            ),
            staticKb = theoryOf(
                fact { iris(5.1, 3.0, 1.4, 0.2, "theory") },
                fact { iris(4.9, 3.0, 1.4, 0.2, "theory") },
                fact { iris(4.5, 3.0, 1.3, 0.2, "theory") },
                fact { iris(4.9, 2.4, 1.4, 0.2, "theory") },
                fact { "attribute"(0, "sepal_length", "type"("double")) },
                fact { "attribute"(1, "sepal_width", "type"("double")) },
                fact { "attribute"(2, "petal_length", "type"("double")) },
                fact { "attribute"(3, "petal_width", "type"("double")) },

                fact {
                    "attribute"(
                        4,
                        "class",
                        "type"("categorical"),
                        listOf("setosa", "versicolor", "virginica")
                    )
                },
                rule {
                    "build_network"(A) impliedBy
                        tupleOf(
                            "input_layer"(4, I),
                            "dense_layer"(I, 40, "activation"("relu"), D),
                            "dense_layer"(D, 20, "activation"("relu"), X),
                            "output_layer"(X, 3, "activation"("softmax"), "loss"("cross_entropy"), O),
                            "network"(O, N),
                            "assert"("registered"(A, N))
                        )
                },
                rule {
                    "train"(N, A, B) impliedBy
                        tupleOf(
                            "schema_from_theory"(S),
                            "normalize"(S, listOf("petal_width", "petal_length", "sepal_width", "sepal_length"), "S2"),
                            "one_hot_encoding"("S2", listOf("class"), "S3"),
                            "transform_process"("S3", T),
                            "load_dataset_from_csv"(TestCSV.iris, D),
                            "fit"(T, D),
                            "transform_dataset"(D, T, tempPath, O),
                            "random_split"(O, 0.8, "Train", "Test"),
                            "accuracy"(N, "Test", 4, 6, A),
                            "train"(N, "Train", 4, 6, 300),
                            "accuracy"(N, "Test", 4, 6, B),
                        )
                },
                rule {
                    "build_tp"(A) impliedBy
                        tupleOf(
                            "schema_from_theory"(S),
                            "normalize"(S, listOf("petal_width", "petal_length", "sepal_width", "sepal_length"), "S2"),
                            "delete_columns"("S2", listOf("class"), "S3"),
                            "transform_process"("S3", T),
                            "load_dataset_from_csv"(TestCSV.iris, D),
                            "fit"(T, D),
                            "assert"("registered"(A, T))

                        )
                },
            )
        )

        val query = tupleOf(
            "build_network"("network"),
            "build_tp"("tp"),
            "registered"("network", N),
            "registered"("tp", T),
            "enhance"("iris", T, N, "class"),
            "train"(N, A, B),
            iris(5.1, 3.0, 1.4, 0.2, "L1"), // setosa
            iris(5.6, 2.9, 3.6, 1.3, "L2"), // versicolor
            "iris"("sepal_length"(5.9), "sepal_width"(3.0), "petal_length"(5.1), "petal_width"(1.8), "L3") // virginica
        )

        solver.solve(query).forEach {
            when (it) {
                is Solution.No -> println("no.\n")
                is Solution.Yes -> {
                    println("yes: ${it.solvedQuery}")
                    for (assignment in it.substitution) {
                        println("\t${assignment.key} / ${assignment.value}")
                    }
                    println()
                }
                is Solution.Halt -> {
                    println("halt: ${it.exception.message}")
                    for (err in it.exception.prologStackTrace) {
                        println("\t $err")
                    }
                }
            }
        }
        println(solver.dynamicKb)
    }
}
