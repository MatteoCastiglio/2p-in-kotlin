package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Integer
import it.unibo.tuprolog.core.Numeric
import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.core.Var
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.impl.network.layers.LogicLayerFactory
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.BinaryRelation
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith
import it.unibo.tuprolog.solve.libs.ml.LogicInputLayer as InputLayerObject

/**input_layer/2
Description = input_layer(Size, Input)
input_layer(+integer, -input_layer)
The result is the creation of an InputLayer I with the given properties and the substitution {Input/I}
Error cases
Size -> instantation_error, type_error(integer)
Runtime errors -> runtime_error

input_layer(?integer, +input_layer)
The result is the substitution {Size/N) where N is the size of Input
Error cases
Input -> type_error(object_ref), instance_type_error(input_layer)
**/
object InputLayer : BinaryRelation.Functional<ExecutionContext>("input_layer") {

    override fun Solve.Request<ExecutionContext>.computeOneSubstitution(
        first: Term,
        second: Term,
    ): Substitution {
        return if (second is Var) {
            ensuringArgumentIsInstantiated(0)
            ensuringArgumentIsNonNegativeInteger(0)
            val size = (first as Integer).intValue.toLong()
            catchingMLRuntimeExceptions {
                val objectReference = ObjectRef.of(LogicLayerFactory.inputLayer(size))
                objectReference.mguWith(second)
            }
        } else {
            ensuringArgumentIsInstantiated(1)
            ensuringArgumentIsInputLayer(1)
            val inputLayer = (second as ObjectRef).`object` as InputLayerObject
            val sizeTerm = Numeric.of(inputLayer.size)
            sizeTerm mguWith first
        }
    }
}
