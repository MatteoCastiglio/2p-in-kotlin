package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Fact
import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicDataSet
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.BinaryRelation
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith

/**
load_dataset_from_theory/2
Description = load_dataset_from_theory(Functor, Dataset)
load_dataset_from_theory(+atom , -dataset)
A Dataset D is created from facts in theory with functor Functor, if data can’t be converted a runtime_error is generated, the filename of a dataset loaded form theory is “theory”. The substitution {Dataset/D} is returned.
Error cases
•	Functor -> instantation_error, type_error(atom)
•	Dataset -> type_error(var)
•	Runtime errors-> runtime
**/
object LoadDataSetFromTheory : BinaryRelation.Functional<ExecutionContext>("load_dataset_from_theory") {

    private fun Solve.Request<ExecutionContext>.getDataFromTheory(name: String): Iterable<Struct> {
        val theory = context.staticKb + context.dynamicKb
        return theory.clauses.asIterable().filterIsInstance<Fact>().map { it.head }.filter { it.functor == name }
    }

    override fun Solve.Request<ExecutionContext>.computeOneSubstitution(
        first: Term,
        second: Term,
    ): Substitution {
        ensuringArgumentIsInstantiated(0)
        ensuringArgumentIsAtom(0)
        ensuringArgumentIsVar(1)
        return catchingMLRuntimeExceptions {
            val dataset = LogicDataSet.fromTheory(getDataFromTheory(first.toString()))
            val objectReference = ObjectRef.of(dataset)
            objectReference.mguWith(second)
        }
    }
}
