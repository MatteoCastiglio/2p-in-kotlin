package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicDataSet
import it.unibo.tuprolog.solve.libs.ml.LogicTransformProcess
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.BinaryRelation
import it.unibo.tuprolog.solve.primitive.Solve

/**
fit/2
Description = fit(TP, Dataset)
fit(+transform_process, +dataset)
TP is fitted with Dataset (Dataset should match TP initial schema)
Side Effects
The internal state of TP is changed by the fitting process.
Error cases
•	TP -> instantation_error, type_error(object_ref), instance_type_error(transform_process)
•	Schema-> instantation_error, type_error(object_ref), instance_type_error(schema)
•	Runtime errors -> runtime_error
**/
object Fit : BinaryRelation.Predicative<ExecutionContext>("fit") {

    override fun Solve.Request<ExecutionContext>.compute(
        first: Term,
        second: Term
    ): Boolean {
        ensuringArgumentIsInstantiated(0)
        ensuringArgumentIsInstantiated(1)
        ensuringArgumentIsTransformProcess(0)
        ensuringArgumentIsDataset(1)
        val tp = (first as ObjectRef).`object` as LogicTransformProcess
        val dataset = (second as ObjectRef).`object` as LogicDataSet
        catchingMLRuntimeExceptions {
            tp.fit(dataset)
            return true
        }
    }
}
