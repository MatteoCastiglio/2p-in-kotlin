package it.unibo.tuprolog.solve.libs.ml.impl.network.layers

import it.unibo.tuprolog.solve.libs.ml.LogicInputLayer

class LogicInputLayerImpl(override val size: Long) : LogicInputLayer
