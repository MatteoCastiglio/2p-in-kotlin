package it.unibo.tuprolog.solve.libs.ml

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.Struct

/**
 *  enum for representing the loss function of a NeuralNetwork
 */
enum class LogicLoss(val struct: Struct) {

    MSE(Struct.of("loss", listOf(Atom.of("mse")))),
    CROSS_ENTROPY(Struct.of("loss", listOf(Atom.of("cross_entropy")))),
    OTHER(Struct.of("loss", listOf(Atom.of("other"))));

    companion object {
        private val map = values().filterNot { it == OTHER }.associateBy(LogicLoss::struct)
        fun fromStruct(struct: Struct) = map[struct]
    }
}
