package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Fact
import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.Attribute
import it.unibo.tuprolog.solve.libs.ml.LogicSchema
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.solve.primitive.UnaryPredicate
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith

/**
schema_from_theory/1
Description = schema_from_theory(Schema)
schema_from_theory(-schema)
A schema S is created according to the description contained in theory. The substitution {Schema/S} is returned.
Error cases
Schema -> type_error(var), runtime_error*
Runtime errors -> runtime_error
*Theory must contain a valid description of a schema, otherwise a runtime_error is produced.
**/
object SchemaFromTheory : UnaryPredicate.Functional<ExecutionContext>("schema_from_theory") {

    private fun Solve.Request<ExecutionContext>.getSchemaInfo(): Iterable<Struct> {
        val theory = context.staticKb + context.dynamicKb
        return theory.clauses.filterIsInstance<Fact>().map { it.head }.filter { it.functor == "attribute" }
    }

    override fun Solve.Request<ExecutionContext>.computeOneSubstitution(first: Term): Substitution {
        ensuringArgumentIsVar(0)
        return catchingMLRuntimeExceptions {
            val schema = LogicSchema.of(Attribute.getAttributes(getSchemaInfo()))
            val schemaRef = ObjectRef.of(schema)
            schemaRef mguWith first
        }
    }
}
