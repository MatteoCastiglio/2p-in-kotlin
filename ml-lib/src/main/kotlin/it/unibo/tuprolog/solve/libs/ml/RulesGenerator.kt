package it.unibo.tuprolog.solve.libs.ml

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.Rule
import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.core.Var
import it.unibo.tuprolog.dsl.prolog
import it.unibo.tuprolog.solve.libs.oop.ObjectRef

object RulesGenerator {
    private fun generateIdRule(name: String, attribute: String, index: String, attributeNames: List<String>): Rule =
        prolog {
            val head = attribute("ID", "Value")
            val body = structOf(
                name,
                attributeNames.map {
                    when (it) {
                        attribute -> attribute("Value")
                        index ->
                            index("ID")
                        else -> Var.of("_")
                    }
                }
            )
            Rule.of(head, body)
        }

    private fun generateIdRules(predicateName: String, attributes: Iterable<Attribute>): List<Rule> =
        try {
            val index = attributes.first { it is IndexAttribute }.name
            val attributeNames = attributes.map { it.name }
            attributeNames.map { generateIdRule(predicateName, it, index, attributeNames) }
        } catch (_: NoSuchElementException) {
            listOf()
        }

    private fun enhancedPrediction(predicateName: String, attributes: Iterable<Attribute>, tp: LogicTransformProcess, target: String, predict: Struct) =
        prolog {
            val args = attributes.map { Var.of(it.name.toUpperCase()) }
            val head = Struct.of(predicateName, args)
            val transform = "transform"(head, ObjectRef.of(tp), "Vector")
            val result = "="(Var.of(target.toUpperCase()), target("Output"))
            Rule.of(head, transform, predict, result)
        }

    private fun enhancedRegression(predicateName: String, attributes: Iterable<Attribute>, tp: LogicTransformProcess, network: LogicNetwork, target: String) =
        prolog {
            val predict = "predict"("Vector", ObjectRef.of(network), "Output")
            enhancedPrediction(predicateName, attributes, tp, target, predict)
        }

    private fun enhancedClassification(predicateName: String, attributes: Iterable<Attribute>, tp: LogicTransformProcess, network: LogicNetwork, target: String, classList: List<String>) =
        prolog {
            val classify = "classify"("Vector", ObjectRef.of(network), listOf(classList.map { Atom.of(it) }), "Output")
            enhancedPrediction(predicateName, attributes, tp, target, classify)
        }

    fun enhance(predicateName: String, tp: LogicTransformProcess, network: LogicNetwork, target: String): List<Rule> {
        val attributes = tp.initialSchema.attributes()
        val ruleList = mutableListOf<Rule>()
        ruleList.addAll(generateIdRules(predicateName, attributes))
        if (attributes.any { it.name == target }) {
            ruleList.add(
                when (val targetAttribute = attributes.first { it.name == target }) {
                    is CategoricalAttribute -> enhancedClassification(
                        predicateName,
                        attributes,
                        tp,
                        network,
                        target,
                        targetAttribute.categories
                    )
                    is DoubleAttribute -> enhancedRegression(
                        predicateName,
                        attributes,
                        tp,
                        network,
                        target
                    )
                    else -> throw SchemaDefinitionException("${targetAttribute.type} is not supported as target type")
                }
            )
            return ruleList
        } else {
            throw SchemaDefinitionException("$target is not an attribute of Schema")
        }
    }
}
