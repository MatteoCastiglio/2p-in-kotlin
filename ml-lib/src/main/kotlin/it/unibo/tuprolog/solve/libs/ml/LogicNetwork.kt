package it.unibo.tuprolog.solve.libs.ml

import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.solve.libs.ml.impl.dataset.LogicDataSetImpl
import it.unibo.tuprolog.solve.libs.ml.impl.network.LogicNetworkImpl

interface LogicNetwork {

    val outputLayer: LogicOutputLayer
    fun classify(input: Struct, classList: List<String>): String
    fun predict(input: Struct): List<Double>
    fun predictDataset(data: LogicDataSet, fileName: String): LogicDataSetImpl
    fun summary(): String
    fun train(dataset: LogicDataSet, labelIndexFrom: Int, labelIndexTo: Int, iterations: Int)
    fun accuracy(dataset: LogicDataSet, labelIndexFrom: Int, labelIndexTo: Int): Double
    fun mse(dataset: LogicDataSet, labelIndex: Int): Double

    companion object {
        fun of(outputLayer: LogicOutputLayer): LogicNetworkImpl {
            return LogicNetworkImpl.of(outputLayer)
        }
    }
}
