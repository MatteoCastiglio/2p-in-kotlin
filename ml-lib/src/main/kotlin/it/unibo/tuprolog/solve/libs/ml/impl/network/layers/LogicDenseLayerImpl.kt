package it.unibo.tuprolog.solve.libs.ml.impl.network.layers

import it.unibo.tuprolog.solve.libs.ml.LogicActivation
import it.unibo.tuprolog.solve.libs.ml.LogicDenseLayer
import it.unibo.tuprolog.solve.libs.ml.LogicLayer
import org.deeplearning4j.nn.conf.layers.BaseLayer
import org.deeplearning4j.nn.conf.layers.FeedForwardLayer as D4JFeedForwardLayer

open class LogicDenseLayerImpl(override val previousLayer: LogicLayer, val layerImpl: BaseLayer) :
    LogicDenseLayer {

    override val activation: LogicActivation
        get() = ActivationConverter.implToLogic((layerImpl as D4JFeedForwardLayer).activationFn)

    override val size: Long
        get() = (layerImpl as D4JFeedForwardLayer).nOut
}
