package it.unibo.tuprolog.solve.libs.ml.impl.network.layers

import it.unibo.tuprolog.solve.libs.ml.LogicLayer
import it.unibo.tuprolog.solve.libs.ml.LogicLoss
import it.unibo.tuprolog.solve.libs.ml.LogicOutputLayer
import org.deeplearning4j.nn.conf.layers.BaseLayer
import org.deeplearning4j.nn.conf.layers.OutputLayer as D4JOutputLayer

class LogicOutputLayerImpl(previousLayer: LogicLayer, layerImpl: BaseLayer) :
    LogicDenseLayerImpl(previousLayer, layerImpl),
    LogicOutputLayer {

    override val loss: LogicLoss
        get() = LossConverter.implToLogic((layerImpl as D4JOutputLayer).lossFn)
}
