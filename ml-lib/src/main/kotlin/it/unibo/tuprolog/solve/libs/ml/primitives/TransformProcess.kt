package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.core.Var
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicSchema
import it.unibo.tuprolog.solve.libs.ml.LogicTransformProcess
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.BinaryRelation
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith

/**
transform_process/2
Description = transform_process(Schema, TP)
transform_process(+schema, -transform_process)
The result is the creation of a TranformProcess T that has Schema as final step schema (Schema keeps track of all previous processing step). The substitution {TP /T} is returned.
Error cases
Schema -> instantation_error, type_error(object_ref), instance_type_error(schema)
Runtime errors -> runtime_error
transform_process(?schema, +transform_process)
The result is the substitution {Schema/S) where S is the schema at the final step of TP.
Error cases
TP -> type_error(object_ref), instance_type_error(transform_process)
 **/
object TransformProcess : BinaryRelation.Functional<ExecutionContext>("transform_process") {
    override fun Solve.Request<ExecutionContext>.computeOneSubstitution(
        first: Term,
        second: Term
    ): Substitution {
        return if (second is Var) {
            catchingMLRuntimeExceptions {
                ensuringArgumentIsInstantiated(0)
                ensuringArgumentIsSchema(0)
                val schemaInRef = (first as ObjectRef).`object` as LogicSchema
                val tp = LogicTransformProcess.fromSchema(schemaInRef)
                val transformProcess = ObjectRef.of(tp)
                transformProcess mguWith second
            }
        } else {
            ensuringArgumentIsInstantiated(1)
            ensuringArgumentIsTransformProcess(1)
            val tp = (second as ObjectRef).`object` as LogicTransformProcess
            return first mguWith ObjectRef.of(tp.finalSchema)
        }
    }
}
