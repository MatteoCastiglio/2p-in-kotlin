package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicDataSet
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.BinaryRelation
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith

/**
load_dataset_from_csv/2
Description = load_dataset_from_csv(Filename, Dataset)
load_dataset_from_csv(+atom , -dataset)
A Dataset D is created from the file located at filename. The name of a dataset loaded from a file is the name of the file. The substitution {Dataset/D} is returned.
Error cases
•	Filename -> instantation_error, type_error(atom)
•	Dataset -> type_error(var)
•	Runtime errors-> runtime_error
**/
object LoadDataSetFromCSV : BinaryRelation.Functional<ExecutionContext>("load_dataset_from_csv") {

    private fun deleteExternalApices(name: String): String =
        if (name[0] == '\'') {
            name.removeSurrounding("'")
        } else {
            name
        }

    override fun Solve.Request<ExecutionContext>.computeOneSubstitution(
        first: Term,
        second: Term,
    ): Substitution {
        ensuringArgumentIsInstantiated(0)
        ensuringArgumentIsAtom(0)
        ensuringArgumentIsVar(1)

        return catchingMLRuntimeExceptions {
            val dataset = LogicDataSet.fromCsv(deleteExternalApices(first.toString()))
            val objectReference = ObjectRef.of(dataset)
            objectReference.mguWith(second)
        }
    }
}
