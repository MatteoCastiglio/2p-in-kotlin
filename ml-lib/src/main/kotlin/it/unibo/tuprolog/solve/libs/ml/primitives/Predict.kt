package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Numeric
import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicNetwork
import it.unibo.tuprolog.solve.libs.ml.impl.schema.StopQueryException
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.solve.primitive.TernaryRelation
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith
import it.unibo.tuprolog.core.List as LogicList

/**
predict/3
Description = predict(Input , Network, Output)
predict(+struct, +network, -struct)
The result is the conversion of input* in a vector and the computation of Network output O with the vector as Input. If Network has only one output neuron O is an Integer, otherwise is a List.
The substitution {O/Output} is returned.
Error cases
Input -> instantation_error, runtime_error*
Network -> instantation_error, type_error(object_ref), instance_type_error(nework)
Output -> type_error(var)
Runtime errors -> runtime_error
* Input must be a struct with any functor and arity equal to number of input neurons, arg must be structs with arity1 and a numeric arg. If Input doesn’t match a runtime_error is returned
**/
object Predict : TernaryRelation.Functional<ExecutionContext>("predict") {

    override fun Solve.Request<ExecutionContext>.computeOneSubstitution(
        first: Term,
        second: Term,
        third: Term,
    ): Substitution {
        ensuringArgumentIsInstantiated(0)
        ensuringArgumentIsInstantiated(0)
        ensuringArgumentIsNetwork(1)
        val network = (second as ObjectRef).`object` as LogicNetwork
        return try {
            catchingMLRuntimeExceptions {
                val result = network.predict(first as Struct)
                return if (result.size > 1) {
                    third mguWith LogicList.of(result.map { Numeric.of(it) })
                } else {
                    third mguWith Numeric.of(result[0])
                }
            }
        } catch (e: StopQueryException) {
            Substitution.failed()
        }
    }
}
