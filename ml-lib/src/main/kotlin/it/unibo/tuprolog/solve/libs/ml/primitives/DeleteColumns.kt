package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.solve.libs.ml.LogicSchema
import it.unibo.tuprolog.solve.libs.ml.LogicTransformProcess

/**
delete_columns/3
Description = delete_columns(SchemaIn, Columns, SchemaOut)
delete_columns(+schema, list, -schema)
A schema S is created as the result of deleting columns listed in Columns from SchemaIn. S will keep track of previous steps (including this one). The substitution {SchemaOut/S} is returned.
Error cases
•	SchemaIn-> instantation_error, type_error(object_ref), instance_type_error(schema)
•	Columns-> instantation_error, type_error(list), type_error(atom)
•	SchemaOut-> type_error(var)
**/
object DeleteColumns : SchemaOperation("delete_columns") {

    override fun getTransformedSchema(schema: LogicSchema, columns: List<String>): LogicSchema {
        return LogicTransformProcess.deleteColumns(schema, columns)
    }
}
