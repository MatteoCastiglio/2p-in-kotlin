package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Integer
import it.unibo.tuprolog.core.Numeric
import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.core.Var
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicActivation
import it.unibo.tuprolog.solve.libs.ml.LogicLayer
import it.unibo.tuprolog.solve.libs.ml.impl.network.layers.LogicLayerFactory
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.QuaternaryRelation
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith
import it.unibo.tuprolog.solve.libs.ml.LogicDenseLayer as DenseLayerObject

/**
dense_layer/4
Description = dense_layer(Previous, Size, Activation, Dense )
dense_layer(+layer, +integer, +activation,  -dense_layer)
The result is the creation of an DenseLayer D with the given properties and the substitution {Dense/D}
Error cases
Previous -> instantation_error, type_error(object_ref), instance_type_error(layer)
Size -> instantation_error, type_error(integer)
Activation -> instantation_error, domain_error(activation)
Runtime errors -> runtime_error

dense_layer(?layer, ?integer, ?activation,  +dense_layer)
The result is the substitution {Previous/P. Size/N, Activation/A) where N is the size of Dense, A the activation and P  the previous_layer
Error cases
Dense -> type_error(object_ref), instance_type_error(dense_layer)
**/
object DenseLayer : QuaternaryRelation.Functional<ExecutionContext>("dense_layer") {

    override fun Solve.Request<ExecutionContext>.computeOneSubstitution(
        first: Term,
        second: Term,
        third: Term,
        fourth: Term
    ): Substitution {
        return if (fourth is Var) {
            ensuringArgumentIsInstantiated(0)
            ensuringArgumentIsInstantiated(1)
            ensuringArgumentIsInstantiated(2)
            ensuringArgumentIsLayer(0)
            ensuringArgumentIsNonNegativeInteger(1)
            ensuringArgumentIsActivation(2)
            val previousLayer = (first as ObjectRef).`object` as LogicLayer
            val size = (second as Integer).intValue.toLong()
            val activation = LogicActivation.fromStruct(third as Struct)!!
            catchingMLRuntimeExceptions {
                val objectReference = ObjectRef.of(LogicLayerFactory.denseLayer(size, activation, previousLayer))
                objectReference.mguWith(fourth)
            }
        } else {
            ensuringArgumentIsInstantiated(3)
            ensuringArgumentIsDenseLayer(3)
            val denseLayer = (fourth as ObjectRef).`object` as DenseLayerObject
            val previousLayerReference = ObjectRef.of(denseLayer.previousLayer)
            val activationTerm = denseLayer.activation.struct
            val sizeTerm = Numeric.of(denseLayer.size)
            (previousLayerReference.mguWith(first) + sizeTerm.mguWith(second) + activationTerm.mguWith(third))
        }
    }
}
