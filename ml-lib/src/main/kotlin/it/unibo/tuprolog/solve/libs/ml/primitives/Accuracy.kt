package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Numeric
import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicDataSet
import it.unibo.tuprolog.solve.libs.ml.LogicNetwork
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.QuinaryRelation
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith

/**
accuracy/5
Description = accuracy(Network, Dataset, IndexFrom, IndexTo, Accuracy)

accuracy(+network, +dataset, +integer, +integer, -integer )
Network is evaluated by computing an accuracy score A on the whole dataset, IndexFrom and IndexTo are used to define which part of the dataset are used as target. The substitution {A/Accuracy} is returned
Error cases
Network -> instantation_error, type_error(object_ref), instance_type_error(nework)
Dataset -> instantation_error, type_error(object_ref), instance_type_error(dataset)
IndexFrom -> instantation_error, type_error(integer)
IndexTo -> instantation_error, type_error(integer)
Accuracy-> type_error(var)
Runtime errors -> runtime_error
**/
object Accuracy : QuinaryRelation.Functional<ExecutionContext>("accuracy") {

    override fun Solve.Request<ExecutionContext>.computeOneSubstitution(
        first: Term,
        second: Term,
        third: Term,
        fourth: Term,
        fifth: Term
    ): Substitution {
        ensuringArgumentIsInstantiated(0)
        ensuringArgumentIsInstantiated(1)
        ensuringArgumentIsInstantiated(2)
        ensuringArgumentIsInstantiated(3)
        ensuringArgumentIsNetwork(0)
        ensuringArgumentIsDataset(1)
        ensuringArgumentIsInteger(2)
        ensuringArgumentIsInteger(3)
        val network = (first as ObjectRef).`object` as LogicNetwork
        val dataset = (second as ObjectRef).`object` as LogicDataSet
        val labelIndexFrom = (third as Numeric).intValue.toInt()
        val labelIndexTo = (fourth as Numeric).intValue.toInt()
        catchingMLRuntimeExceptions {
            val result = network.accuracy(dataset, labelIndexFrom, labelIndexTo)
            return fifth mguWith Numeric.of(result)
        }
    }
}
