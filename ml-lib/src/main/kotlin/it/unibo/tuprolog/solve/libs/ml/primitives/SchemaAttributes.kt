package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.core.Var
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.Attribute
import it.unibo.tuprolog.solve.libs.ml.LogicSchema
import it.unibo.tuprolog.solve.libs.ml.impl.schema.LogicSchemaImpl
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.BinaryRelation
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith
import it.unibo.tuprolog.core.List as LogicList

/**
schema_attributes/2
Description = schema_attriutes (Attributes, Schema)
schema_attributes (+list, -schema)
A schema S is created according to the description contained in Attributes. The substitution {Schema/S} is returned.
Error cases
Attributes-> instantation_error, type_error(list), runtime_error*
Runtime errors -> runtime_error
*Theory must contain a valid description of a schema (see introduction), otherwise a runtime_error is produced.
schema_attributes (?list, +schema)
A list L containing a description of Schema is created. The substitution {Attributes/L} is returned.
Error cases
Schema -> instantation_error, type_error(object_ref), instance_type_error(schema
**/
object SchemaAttributes : BinaryRelation.Functional<ExecutionContext>("schema_attributes") {

    override fun Solve.Request<ExecutionContext>.computeOneSubstitution(
        first: Term,
        second: Term
    ): Substitution {
        return if (second is Var) {
            ensuringArgumentIsInstantiatedList(0)
            catchingMLRuntimeExceptions {
                val schemaInfo = (first as LogicList).toList()
                val schema = LogicSchemaImpl.of(Attribute.getAttributes(schemaInfo))
                val schemaRef = ObjectRef.of(schema)
                schemaRef.mguWith(second)
            }
        } else {
            ensuringArgumentIsInstantiated(1)
            ensuringArgumentIsSchema(1)
            val schema = (second as ObjectRef).`object` as LogicSchema
            val logicList = schema.logicList()
            logicList.mguWith(first)
        }
    }
}
