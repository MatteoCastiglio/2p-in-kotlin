package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Numeric
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicDataSet
import it.unibo.tuprolog.solve.libs.ml.LogicNetwork
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.QuinaryRelation
import it.unibo.tuprolog.solve.primitive.Solve

/**
train/5
Description = train(Network, Dataset, IndexFrom, IndexTo, Epochs)

train(+network, +dataset, +integer, +integer, +integer )
Network is trained for Epochs iteration with the whole dataset, IndexFrom and IndexTo are used to define which part of the dataset are used as target. Loss function used is a property of the network.
Side Effects
The internal state of network is changed by the training process
Error cases
Network -> instantation_error, type_error(object_ref), instance_type_error(nework)
Dataset -> instantation_error, type_error(object_ref), instance_type_error(dataset)
IndexFrom -> instantation_error, type_error(integer)
IndexTo -> instantation_error, type_error(integer)
Epochs-> instantation_error, type_error(integer)
Runtime errors -> runtime_error
Variants : train/4
train( Network, Dataset, Index, Epochs) :- train( Network, Dataset, Index, Index, Epochs)
**/
object Train : QuinaryRelation.Predicative<ExecutionContext>("train") {
    override fun Solve.Request<ExecutionContext>.compute(
        first: Term,
        second: Term,
        third: Term,
        fourth: Term,
        fifth: Term
    ): Boolean {
        ensuringArgumentIsInstantiated(0)
        ensuringArgumentIsInstantiated(1)
        ensuringArgumentIsInstantiated(2)
        ensuringArgumentIsInstantiated(3)
        ensuringArgumentIsInstantiated(4)
        ensuringArgumentIsNetwork(0)
        ensuringArgumentIsDataset(1)
        ensuringArgumentIsInteger(2)
        ensuringArgumentIsInteger(3)
        ensuringArgumentIsInteger(4)
        val network = (first as ObjectRef).`object` as LogicNetwork
        val dataset = (second as ObjectRef).`object` as LogicDataSet
        val labelIndexFrom = (third as Numeric).intValue.toInt()
        val labelIndexTo = (fourth as Numeric).intValue.toInt()
        val epochs = (fifth as Numeric).intValue.toInt()
        return catchingMLRuntimeExceptions {
            network.train(dataset, labelIndexFrom, labelIndexTo, epochs)
            true
        }
    }
}
