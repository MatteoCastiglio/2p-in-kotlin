package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.solve.libs.ml.LogicSchema
import it.unibo.tuprolog.solve.libs.ml.LogicTransformProcess

/**
one_hot_encoding/3
Description = one_hot_encoding(SchemaIn, Columns, SchemaOut)
one_hot_encoding (+schema, list, -schema)
A schema S is created as the result of a one hot encoding step on SchemaIn applied on columns listed in Columns. S will keep track of previous steps (including this one). The substitution {SchemaOut/S} is returned.
Error cases
•	SchemaIn-> instantation_error, type_error(object_ref), instance_type_error(schema)
•	Columns-> instantation_error, type_error(list), type_error(atom)
•	SchemaOut-> type_error(var)
**/
object OneHotEncoding : SchemaOperation("one_hot_encoding") {
    override fun getTransformedSchema(schema: LogicSchema, columns: List<String>): LogicSchema {
        return LogicTransformProcess.oneHotEncoding(schema, columns)
    }
}
