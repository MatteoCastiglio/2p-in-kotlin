package it.unibo.tuprolog.solve.libs.ml

class SchemaDefinitionException(s: String) : Exception(s)
