package it.unibo.tuprolog.solve.libs.ml.exception

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.core.TermFormatter
import it.unibo.tuprolog.core.ToTermConvertible
import it.unibo.tuprolog.core.format
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.Signature
import it.unibo.tuprolog.solve.exception.PrologError

class InstanceTypeError(
    message: String? = null,
    cause: Throwable? = null,
    contexts: Array<ExecutionContext>,
    private val expectedType: Expected,
    private val culprit: Term,
    extraData: Term? = null
) : PrologError(message, cause, contexts, Atom.of(typeFunctor), extraData) {

    constructor(
        message: String? = null,
        cause: Throwable? = null,
        context: ExecutionContext,
        expectedType: Expected,
        culprit: Term,
        extraData: Term? = null
    ) : this(message, cause, arrayOf(context), expectedType, culprit, extraData)

    override fun updateContext(newContext: ExecutionContext): InstanceTypeError =
        InstanceTypeError(message, cause, contexts.setFirst(newContext), expectedType, culprit, extraData)

    override fun pushContext(newContext: ExecutionContext): InstanceTypeError =
        InstanceTypeError(message, cause, contexts.addLast(newContext), expectedType, culprit, extraData)

    override val type: Struct by lazy { Struct.of(super.type.functor, expectedType.toTerm(), culprit) }

    companion object {

/*
        fun of(
            context: ExecutionContext,
            expectedType: Expected,
            actualValue: Term,
            message: String
        ) = message(message) { m, extra ->
            InstanceTypeError(
                message = m,
                context = context,
                expectedType = expectedType,
                culprit = actualValue,
                extraData = extra
            )
        }

        fun forArgumentList(
            context: ExecutionContext,
            procedure: Signature,
            expectedType: Expected,
            culprit: Term,
            index: Int? = null
        ) = message(
            (index?.let { "The $it-th argument" } ?: "An argument") +
                " of `${procedure.pretty()}` should be a list of `$expectedType`, " +
                "but `${culprit.pretty()}` has been provided instead"
        ) { m, extra ->
            InstanceTypeError(
                message = m,
                context = context,
                expectedType = expectedType,
                culprit = culprit,
                extraData = extra
            )
        }

 */

        fun forArgument(
            context: ExecutionContext,
            procedure: Signature,
            expectedType: Expected,
            culprit: Term,
            index: Int? = null
        ): InstanceTypeError {
            val msg = (index?.let { "The $it-th argument" } ?: "An argument") +
                " of `${procedure.toIndicator()}` should be a `$expectedType`, " +
                "but `${culprit.format(TermFormatter.prettyVariables())}` has been provided instead"
            return InstanceTypeError(message = msg, context = context, expectedType = expectedType, culprit = culprit, extraData = Atom.of(msg))
        }

/*
        fun forGoal(
            context: ExecutionContext,
            procedure: Signature,
            expectedType: Expected,
            culprit: Term
        ) = message(
            "Subgoal `${culprit.pretty()}` of ${procedure.pretty()} is not a $expectedType term"
        ) { m, extra ->
            InstanceTypeError(
                message = m,
                context = context,
                expectedType = expectedType,
                culprit = culprit,
                extraData = extra
            )
        }
*/
        /** The type error Struct functor */
        const val typeFunctor = "instance_type_error"
    }

    /**
     * A class describing the expected type whose absence caused the error
     */
    enum class Expected : ToTermConvertible {
        LAYER,
        INPUT_LAYER,
        DENSE_LAYER,
        OUTPUT_LAYER,
        NETWORK,
        DATASET,
        SCHEMA,
        TRANSFORM_PROCESS;

        /**
         * The type expected string description
         */
        private val type: String by lazy { name.toLowerCase() }

        /** A function to transform the type to corresponding [Atom] representation */
        override fun toTerm(): Atom = Atom.of(type)

        override fun toString(): String = type

        companion object {
            /** Returns the Expected instance described by [type]; creates a new instance only if [type] was not predefined */

            fun of(type: String): Expected = valueOf(type.toUpperCase())

            /** Gets [Expected] instance from [term] representation, if possible */

            fun fromTerm(term: Term): Expected? = when (term) {
                is Atom -> of(term.value)
                else -> null
            }
        }
    }
}
