package it.unibo.tuprolog.solve.libs.ml.impl.network

class NetworkException(message: String) : Exception(message.substringBefore("This configuration"))
