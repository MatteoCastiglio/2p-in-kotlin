package it.unibo.tuprolog.solve.libs.ml.impl.dataset

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.Integer
import it.unibo.tuprolog.core.Real
import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.libs.ml.Attribute
import it.unibo.tuprolog.solve.libs.ml.CategoricalAttribute
import it.unibo.tuprolog.solve.libs.ml.DoubleAttribute
import it.unibo.tuprolog.solve.libs.ml.IndexAttribute
import it.unibo.tuprolog.solve.libs.ml.IntegerAttribute
import it.unibo.tuprolog.solve.libs.ml.LogicSchema
import it.unibo.tuprolog.solve.libs.ml.SchemaDefinitionException
import it.unibo.tuprolog.solve.libs.ml.StringAttribute
import org.datavec.api.writable.DoubleWritable
import org.datavec.api.writable.IntWritable
import org.datavec.api.writable.Text
import org.datavec.api.writable.Writable

object RecordConverter {

    private fun writableToTerm(attribute: Attribute, x: Writable): Term {
        return when (attribute) {
            is IndexAttribute -> Integer.of(x.toInt())
            is IntegerAttribute -> Integer.of(x.toInt())
            is DoubleAttribute -> Real.of(x.toDouble())
            is StringAttribute -> Atom.of(x.toString())
            is CategoricalAttribute -> Atom.of(x.toString())
            else -> TODO()
        }
    }

    private fun termToWritable(x: Term): Writable {
        return when (x) {
            is Atom -> Text(x.toString())
            is Integer -> IntWritable(x.value.toInt())
            is Real -> DoubleWritable(x.value.toDouble())
            else -> throw SchemaDefinitionException("invalid term: $x")
        }
    }

    fun recordFromStruct(struct: Struct): List<Writable> {
        val args = struct.args
        val argsValues = args.map { it as Struct }.map { it[0] }
        args.forEach {
            if (it !is Struct || it.arity != 1) {
                throw SchemaDefinitionException("$it must be a struct with one argument")
            }
        }
        return argsValues.map { termToWritable(it) }
    }

    fun recordFromStructWithSchema(struct: Struct, schema: LogicSchema): List<Writable> {
        val args = struct.args
        val attributesNames = schema.attributesNames()
        val argsNumber = args.size
        val attributesCount = attributesNames.count()
        if (argsNumber != attributesCount) {
            throw SchemaDefinitionException(
                "schema does not match data: args number in input = $argsNumber, " +
                    "attributes in schema = $attributesCount"
            )
        }

        val argsNames = args.map { it as Struct }.map { it.functor }
        if (argsNames != attributesNames) {
            throw SchemaDefinitionException("attributes names don't match the schema: argNames: $argsNames, attributesNames: $attributesNames ")
        }

        return recordFromStruct(struct)
    }

    fun structFromRecord(record: List<Writable>, name: String, schema: LogicSchema): Struct {
        val attributes = schema.attributes()
        val recordSize = record.size
        val attributesCount = attributes.count()
        if (recordSize != attributesCount) {
            throw SchemaDefinitionException(
                "schema does not match data: record size = $recordSize, " +
                    "attributes in schema = $attributesCount"
            )
        }
        val args = (attributes zip record).map { (attribute, value) ->
            Struct.of(
                attribute.name,
                writableToTerm(attribute, value)
            )
        }
        return Struct.of(name, args)
    }
}
