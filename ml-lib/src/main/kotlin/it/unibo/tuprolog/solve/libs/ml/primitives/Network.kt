package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.core.Var
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicNetwork
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.BinaryRelation
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith
import it.unibo.tuprolog.solve.libs.ml.LogicOutputLayer as OutputLayerObject

/**
 *
network/2
Description = network(Output, Network )
network(+output_layer, -network)
The result is the creation of an Network N with the given properties and the substitution {Network/N}
Error cases
Output -> instantation_error, type_error(object_ref), instance_type_error(output_layer)
Runtime errors -> runtime_error
network(?output_layer, +network)
The result is the substitution {Output/O) where O is the OutputLayer
Error cases
Network -> type_error(object_ref), instance_type_error(network)
**/
object Network : BinaryRelation.Functional<ExecutionContext>("network") {

    override fun Solve.Request<ExecutionContext>.computeOneSubstitution(
        first: Term,
        second: Term,
    ): Substitution {
        return if (second is Var) {
            ensuringArgumentIsInstantiated(0)
            ensuringArgumentIsOutputLayer(0)
            val outputLayer = (first as ObjectRef).`object` as OutputLayerObject
            catchingMLRuntimeExceptions {
                val network = ObjectRef.of(LogicNetwork.of(outputLayer))
                network.mguWith(second)
            }
        } else {
            ensuringArgumentIsInstantiated(1)
            ensuringArgumentIsNetwork(1)
            val network = (second as ObjectRef).`object` as LogicNetwork
            first.mguWith(ObjectRef.of(network.outputLayer))
        }
    }
}
