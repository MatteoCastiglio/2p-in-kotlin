package it.unibo.tuprolog.solve.libs.ml.impl.dataset

import org.datavec.api.writable.Writable

class DatasetBatchIterator(private val batchSize: Int, val dataset: LogicDataSetImpl) : Iterator<List<List<Writable>>> {

    override fun hasNext(): Boolean {
        return dataset.hasNext()
    }

    override fun next(): List<List<Writable>> {
        return dataset.nextBatch(batchSize)
    }
}
