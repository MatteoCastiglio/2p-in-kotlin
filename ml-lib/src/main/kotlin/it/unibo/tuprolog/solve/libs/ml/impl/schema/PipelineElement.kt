package it.unibo.tuprolog.solve.libs.ml.impl.schema

import org.datavec.api.transform.TransformProcess
import org.datavec.api.transform.analysis.DataAnalysis
import org.datavec.api.transform.schema.Schema

interface PipelineElement {

    fun getTransformedSchema(schema: Schema): Schema

    fun operation(builder: TransformProcess.Builder, da: DataAnalysis)
}
