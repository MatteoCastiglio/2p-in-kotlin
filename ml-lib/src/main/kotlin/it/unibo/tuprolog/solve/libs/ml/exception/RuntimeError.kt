package it.unibo.tuprolog.solve.libs.ml.exception

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.core.ToTermConvertible
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.Signature
import it.unibo.tuprolog.solve.exception.PrologError

class RuntimeError(
    message: String? = null,
    cause: Throwable? = null,
    contexts: Array<ExecutionContext>,
    private val category: Category,
    extraData: Term? = null
) : PrologError(message, cause, contexts, Atom.of(typeFunctor), extraData) {

    constructor(
        message: String? = null,
        cause: Throwable? = null,
        context: ExecutionContext,
        category: Category,
        extraData: Term? = null
    ) : this(message, cause, arrayOf(context), category, extraData)

    override fun updateContext(newContext: ExecutionContext): RuntimeError =
        RuntimeError(message, cause, contexts.setFirst(newContext), category, extraData)

    override fun pushContext(newContext: ExecutionContext): RuntimeError =
        RuntimeError(message, cause, contexts.addLast(newContext), category, extraData)

    override val type: Struct by lazy { Struct.of(super.type.functor) }

    companion object {

        fun of(
            context: ExecutionContext,
            procedure: Signature,
            category: Category,
            message: String
        ): RuntimeError {
            val msg = "${procedure.toIndicator()}: " + message
            return RuntimeError(message = msg, context = context, category = category, extraData = Atom.of(msg))
        }

        /** The type error Struct functor */
        const val typeFunctor = "runtime_error"
    }

    /**
     * A class describing the expected type whose absence caused the error
     */
    enum class Category : ToTermConvertible {
        NETWORK,
        SCHEMA,
        DATASET,
        TRANSFORM_PROCESS,
        RUNTIME;

        /**
         * The type expected string description
         */
        private val type: String by lazy { name.toLowerCase() }

        /** A function to transform the type to corresponding [Atom] representation */
        override fun toTerm(): Atom = Atom.of(type)

        override fun toString(): String = type

        companion object {
            /** Returns the Expected instance described by [type]; creates a new instance only if [type] was not predefined */

            fun of(type: String): Category = valueOf(type.toUpperCase())

            /** Gets [Category] instance from [term] representation, if possible */

            fun fromTerm(term: Term): Category? = when (term) {
                is Atom -> of(term.value)
                else -> null
            }
        }
    }
}
