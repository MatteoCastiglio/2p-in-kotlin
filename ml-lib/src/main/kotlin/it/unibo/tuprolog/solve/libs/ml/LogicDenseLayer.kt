package it.unibo.tuprolog.solve.libs.ml

interface LogicDenseLayer : LogicLayer {
    val previousLayer: LogicLayer
    val activation: LogicActivation
}
