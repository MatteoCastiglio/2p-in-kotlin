package it.unibo.tuprolog.solve.libs.ml

import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.solve.libs.ml.impl.dataset.LogicDataSetImpl
import it.unibo.tuprolog.solve.libs.ml.impl.schema.LogicSchemaImpl
import it.unibo.tuprolog.solve.libs.ml.impl.schema.LogicTransformProcessImpl

interface LogicTransformProcess {

    val initialSchema: LogicSchema
    val finalSchema: LogicSchema
    fun fit(dataIn: LogicDataSet)
    fun transformDataset(dataIn: LogicDataSet, fileName: String): LogicDataSetImpl
    fun transform(input: Struct): Struct

    companion object {
        fun fromSchema(schema: LogicSchema): LogicTransformProcessImpl =
            LogicTransformProcessImpl.fromSchema(schema)

        fun normalize(schema: LogicSchema, columns: List<String>): LogicSchemaImpl =
            LogicTransformProcessImpl.normalize(schema, columns)

        fun oneHotEncoding(schema: LogicSchema, columns: List<String>): LogicSchemaImpl =
            LogicTransformProcessImpl.oneHotEncoding(schema, columns)

        fun deleteColumns(schema: LogicSchema, columns: List<String>): LogicSchemaImpl =
            LogicTransformProcessImpl.deleteColumns(schema, columns)
    }
}
