package it.unibo.tuprolog.solve.libs.ml

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.Struct

/**
 *  class for representing the activation function of a NeuralNetwork Layers
 */

enum class LogicActivation(val struct: Struct) {

    RELU(Struct.of("activation", listOf(Atom.of("relu")))),
    TANH(Struct.of("activation", listOf(Atom.of("tanh")))),
    SIGMOID(Struct.of("activation", listOf(Atom.of("sigmoid")))),
    SOFTMAX(Struct.of("activation", listOf(Atom.of("softmax")))),
    IDENTITY(Struct.of("activation", listOf(Atom.of("identity")))),
    OTHER(Struct.of("activation", listOf(Atom.of("other"))));

    companion object {
        private val map = values().filterNot { it == OTHER }.associateBy(LogicActivation::struct)
        fun fromStruct(struct: Struct) = map[struct]
    }
}
