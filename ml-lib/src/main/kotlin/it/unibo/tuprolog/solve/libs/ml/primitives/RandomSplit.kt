package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Real
import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicDataSet
import it.unibo.tuprolog.solve.libs.ml.exception.RuntimeError
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.QuaternaryRelation
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith
import org.gciatto.kt.math.BigDecimal

/**
random_split/4
Description = random_split(DataIn, Ratio, DataOut1, DataOut2)
random_split(+dataset ,+double, -dataset, dataset)
DataIn is split into two datasets DOut1 and Dout2 where Dout1 has Ratio records of DataIn and Dout2 1-Ratio (if Ratio is not a number between 0 and 1 a runtime_error is generated). The substitution {Dout1/DataOut1, Dout2/DataOut2} is returned.
Side Effects
DataOut1 and DataOut2 are physically written in csv format in files located respectively at XSplit1.csv and XSplit2.csv where X is the name of DataIn.
Error cases
•	DataIn -> instantation_error, type_error(object_ref), instance_type_error(dataset)
•	Ratio -> instantation_error, type_error(double), runtime_error
•	DataOut1 -> type_error(var)
•	DataOut2 -> type_error(var)
•	Runtime errors-> runtime
**/
object RandomSplit : QuaternaryRelation.Functional<ExecutionContext>("random_split") {

    override fun Solve.Request<ExecutionContext>.computeOneSubstitution(
        first: Term,
        second: Term,
        third: Term,
        fourth: Term
    ): Substitution {
        ensuringArgumentIsInstantiated(0)
        ensuringArgumentIsInstantiated(1)
        ensuringArgumentIsDataset(0)
        ensuringArgumentIsNumeric(1)
        ensuringArgumentIsVar(2)
        ensuringArgumentIsVar(3)
        val dataIn = (first as ObjectRef).`object` as LogicDataSet
        val ratio = (second as Real).decimalValue
        if (ratio >= BigDecimal.ONE || ratio <= BigDecimal.ZERO) {
            throw RuntimeError.of(
                context,
                signature,
                RuntimeError.Category.RUNTIME,
                "ratio must be a value between 0 and 1"
            )
        }
        return catchingMLRuntimeExceptions {
            val (dataTrain, dataTest) = dataIn.split(ratio.toDouble())
            (ObjectRef.of(dataTrain) mguWith third) + (ObjectRef.of(dataTest) mguWith fourth)
        }
    }
}
