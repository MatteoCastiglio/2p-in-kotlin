package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Integer
import it.unibo.tuprolog.core.Numeric
import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.core.Var
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicActivation
import it.unibo.tuprolog.solve.libs.ml.LogicLayer
import it.unibo.tuprolog.solve.libs.ml.LogicLoss
import it.unibo.tuprolog.solve.libs.ml.impl.network.layers.LogicLayerFactory
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.QuinaryRelation
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith
import it.unibo.tuprolog.solve.libs.ml.LogicOutputLayer as OutputLayerObject

/**
output_layer/5
Description = output_layer(Previous, Size, Activation, Loss, Output )
output_layer(+layer, +integer, +activation, +loss,  -output_layer)
The result is the creation of an OutputLayer O with the given properties and the substitution {Output/O}
Error cases
Previous -> instantation_error, type_error(object_ref), instance_type_error(layer)
Size -> instantation_error, type_error(integer)
Activation -> instantation_error, domain_error(activation)
Loss -> instantation_error, domain_error(loss)
Runtime errors -> runtime_error
output_layer (?layer, ?integer, ?activation, ?loss, +dense_layer)
The result is the substitution {Previous/P. Size/N, Activation/A, Loss/L) where N is the size of Dense, A the activation ,L the loss, and P the previous_layer
Error cases
Output -> type_error(object_ref), instance_type_error(output_layer)
**/
object OutputLayer : QuinaryRelation.Functional<ExecutionContext>("output_layer") {

    override fun Solve.Request<ExecutionContext>.computeOneSubstitution(
        first: Term,
        second: Term,
        third: Term,
        fourth: Term,
        fifth: Term
    ): Substitution {
        return if (fifth is Var) {
            ensuringArgumentIsInstantiated(0)
            ensuringArgumentIsInstantiated(1)
            ensuringArgumentIsInstantiated(2)
            ensuringArgumentIsInstantiated(3)
            ensuringArgumentIsLayer(0)
            ensuringArgumentIsNonNegativeInteger(1)
            ensuringArgumentIsActivation(2)
            ensuringArgumentIsLoss(3)

            val previousLayer = (first as ObjectRef).`object` as LogicLayer
            val size = (second as Integer).intValue.toLong()
            val activation = LogicActivation.fromStruct(third as Struct)!!
            val loss = LogicLoss.fromStruct(fourth as Struct)!!
            return catchingMLRuntimeExceptions {
                val objectReference =
                    ObjectRef.of(LogicLayerFactory.outputLayer(size, activation, loss, previousLayer))
                objectReference.mguWith(fifth)
            }
        } else {
            ensuringArgumentIsInstantiated(4)
            ensuringArgumentIsOutputLayer(4)
            val outputLayer = (fifth as ObjectRef).`object` as OutputLayerObject
            val previousLayerReference = ObjectRef.of(outputLayer.previousLayer)
            val activationTerm = outputLayer.activation.struct
            val lossTerm = outputLayer.loss.struct
            val sizeTerm = Numeric.of(outputLayer.size)
            (
                previousLayerReference.mguWith(first) + sizeTerm.mguWith(second) +
                    activationTerm.mguWith(third) + lossTerm.mguWith(fourth)
                )
        }
    }
}
