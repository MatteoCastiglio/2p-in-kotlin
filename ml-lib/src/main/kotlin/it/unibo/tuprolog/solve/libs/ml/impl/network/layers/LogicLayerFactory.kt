package it.unibo.tuprolog.solve.libs.ml.impl.network.layers

import it.unibo.tuprolog.solve.libs.ml.LogicActivation
import it.unibo.tuprolog.solve.libs.ml.LogicDenseLayer
import it.unibo.tuprolog.solve.libs.ml.LogicInputLayer
import it.unibo.tuprolog.solve.libs.ml.LogicLayer
import it.unibo.tuprolog.solve.libs.ml.LogicLoss
import it.unibo.tuprolog.solve.libs.ml.LogicOutputLayer
import it.unibo.tuprolog.solve.libs.ml.impl.network.NetworkException
import org.deeplearning4j.exception.DL4JInvalidConfigException
import org.deeplearning4j.nn.conf.layers.DenseLayer as D4JDenseLayer
import org.deeplearning4j.nn.conf.layers.OutputLayer as D4JOutputLayer
import org.nd4j.linalg.activations.Activation as D4JActivation
import org.nd4j.linalg.lossfunctions.LossFunctions.LossFunction as D4JLoss

object LogicLayerFactory {

    private fun getDense(nIn: Long, nOut: Long, activation: LogicActivation): D4JDenseLayer {
        val dl4jActivation: D4JActivation = ActivationConverter.logicToImpl(activation)
        try {
            return D4JDenseLayer.Builder().nIn(nIn).nOut(nOut).activation(dl4jActivation).build()
        } catch (e: DL4JInvalidConfigException) {
            throw NetworkException(e.message!!)
        }
    }

    private fun getOutput(nIn: Long, nOut: Long, activation: LogicActivation, loss: LogicLoss): D4JOutputLayer {
        val d4jactivation: D4JActivation = ActivationConverter.logicToImpl(activation)
        val d4jLoss: D4JLoss = LossConverter.logicToImpl(loss)
        try {
            return D4JOutputLayer.Builder(d4jLoss).nIn(nIn).nOut(nOut).activation(d4jactivation).build()
        } catch (e: DL4JInvalidConfigException) {
            throw NetworkException(e.message!!)
        }
    }

    fun inputLayer(
        size: Long,
    ): LogicInputLayer {
        return LogicInputLayerImpl(size)
    }

    fun denseLayer(
        size: Long,
        activation: LogicActivation,
        previousLayer: LogicLayer
    ): LogicDenseLayer {
        return LogicDenseLayerImpl(
            previousLayer,
            getDense(previousLayer.size, size, activation)
        )
    }

    fun outputLayer(
        size: Long,
        activation: LogicActivation,
        loss: LogicLoss,
        previousLayer: LogicLayer,
    ): LogicOutputLayer {
        return LogicOutputLayerImpl(
            previousLayer,
            getOutput(previousLayer.size, size, activation, loss)
        )
    }
}
