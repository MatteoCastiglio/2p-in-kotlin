package it.unibo.tuprolog.solve.libs.ml

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.Integer
import it.unibo.tuprolog.core.Numeric
import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.core.List as LogicList

/**
 * schema description
 * for string,integer,double
 * attribute(index, name, type):
 * e.g attribute(index, petal_width, double):
 * for categorical
 * attribute(index, name, type(categorical), categories)
 * e.g  attribute(class, type(categorical), [setosa, versicolor])
 * ```
 * types:
 * type(index)
 * type(string)
 * type(integer)
 * type(double)
 * type(categorical)
 */

interface Attribute {
    val index: Int
    val name: String
    val type: String
    val default: Term

    companion object {

        private fun checkNoMoreThanOneID(structs: Iterable<Struct>) {
            val numberOfID = structs.map { (it[2] as Struct).args[0] }.filter { it.toString() == "index" }.count()
            if (numberOfID > 1) {
                throw SchemaDefinitionException("too many attributes with type(index)")
            }
        }

        private fun checkAllIndexDefined(structs: Iterable<Struct>) {
            val indexList = structs.map { (it[0] as Integer).intValue.toInt() }.sorted()
            if (!indexList.zip(IntRange(0, indexList.maxOrNull()!!).asIterable()).all { (x, y) -> x == y }) {
                throw SchemaDefinitionException("missing indices")
            }
        }

        private fun convertStruct(struct: Struct): Attribute {
            if (struct.functor != "attribute") {
                throw(SchemaDefinitionException("$struct:functor must be attribute"))
            }
            if (struct.arity < 3) {
                throw(SchemaDefinitionException("$struct:invalid structure"))
            }
            val index = (struct[0] as Integer).intValue.toInt()
            val type = struct[2].toString()
            val name = struct[1].toString()
            return when (type) {
                "type(index)" -> IndexAttribute(index, name)
                "type(integer)" -> IntegerAttribute(index, name)
                "type(double)" -> DoubleAttribute(index, name)
                "type(string)" -> StringAttribute(index, name)
                "type(categorical)" -> {
                    if (struct.arity < 4) {
                        throw(SchemaDefinitionException("$struct:invalid structure"))
                    }
                    CategoricalAttribute(
                        index,
                        name,
                        (struct[3] as it.unibo.tuprolog.core.List).toList().map { it.toString() }
                    )
                }
                else -> throw(SchemaDefinitionException("$struct: invalid type"))
            }
        }

        private fun convertAttribute(attribute: Attribute): Struct {
            val functor = "type"
            val name = attribute.name
            val index = attribute.index
            val type = when (attribute) {
                is IndexAttribute -> Struct.of(functor, Atom.of("index"))
                is IntegerAttribute -> Struct.of(functor, Atom.of("integer"))
                is DoubleAttribute -> Struct.of(functor, Atom.of("double"))
                is StringAttribute -> Struct.of(functor, Atom.of("string"))
                is CategoricalAttribute -> Struct.of(functor, Atom.of("categorical"))
                else -> TODO()
            }
            return if (attribute is CategoricalAttribute) {
                Struct.of(
                    "attribute",
                    Numeric.of(index),
                    Atom.of(name),
                    type,
                    it.unibo.tuprolog.core.List.of(attribute.categories.map { Atom.of(it.replace("'", "")) })
                )
            } else {
                Struct.of("attribute", Numeric.of(index), Atom.of(name), type)
            }
        }

        fun getAttributes(terms: Iterable<Term>): List<Attribute> {
            val structs = terms.map { it as Struct }
            checkAllIndexDefined(structs)
            checkNoMoreThanOneID(structs)
            structs.sortedBy { (it[0] as Integer).intValue.toInt() }
            return structs.map { convertStruct(it) }
        }

        fun logicList(attributes: Iterable<Attribute>): LogicList =
            LogicList.of(attributes.map { convertAttribute(it) })

        fun termList(attributes: Iterable<Attribute>): List<Term> =
            attributes.map { convertAttribute(it) }
    }
}
