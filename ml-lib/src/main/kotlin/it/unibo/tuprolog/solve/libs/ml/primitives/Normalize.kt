package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.solve.libs.ml.LogicSchema
import it.unibo.tuprolog.solve.libs.ml.LogicTransformProcess

/**
normalize/3
Description = normalize(SchemaIn, Columns, SchemaOut)
normalize (+schema, list, -schema)
A schema S is created as the result of a normalization(standardization) step on SchemaIn applied on columns listed in Columns.
S will keep track of previous steps (including this one). The substitution {SchemaOut/S} is returned.
*If a normalization step is part of a transform process, a fitting step will be required.
Error cases
SchemaIn-> instantation_error, type_error(object_ref), instance_type_error(schema)
Columns-> instantation_error, type_error(list), type_error(atom)
SchemaOut-> type_error(var)
**/
object Normalize : SchemaOperation("normalize") {
    override fun getTransformedSchema(schema: LogicSchema, columns: List<String>): LogicSchema {
        return LogicTransformProcess.normalize(schema, columns)
    }
}
