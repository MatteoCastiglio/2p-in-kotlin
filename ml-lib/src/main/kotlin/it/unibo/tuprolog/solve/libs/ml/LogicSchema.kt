package it.unibo.tuprolog.solve.libs.ml

import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.libs.ml.impl.schema.LogicSchemaImpl
import it.unibo.tuprolog.core.List as LogicList

interface LogicSchema {

    fun logicList(): LogicList
    fun termList(): List<Term>
    fun attributes(): List<Attribute>
    fun attributesNames(): List<String>

    companion object {
        fun of(attributes: List<Attribute>): LogicSchemaImpl =
            LogicSchemaImpl.of(attributes)
    }
}
