package it.unibo.tuprolog.solve.libs.ml

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.Term

data class CategoricalAttribute(override val index: Int, override val name: String, val categories: List<String>) :
    Attribute {

    override val type: String
        get() = "Categorical"

    override val default: Term
        get() = Atom.of(categories[0])
}
