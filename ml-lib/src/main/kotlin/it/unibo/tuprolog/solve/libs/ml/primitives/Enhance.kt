package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicNetwork
import it.unibo.tuprolog.solve.libs.ml.LogicTransformProcess
import it.unibo.tuprolog.solve.libs.ml.RulesGenerator
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.QuaternaryRelation
import it.unibo.tuprolog.solve.primitive.Solve

/**
enhance/4
Description = enhance(Functor, TP, Network, Target)
enhance(+atom ,+transform_process, +network, +atom)
The result is the assertion of generated rules that allow the prediction of target attribute exploiting a TransformProcess and a Network.
Side Effects
Rules are generated based on structs that have Functor as functor and correspond to the initial schema of TP. Two groups of rules are generated: rules that support retrieving records using ID and rules that support prediction of target attribute. Rules of the first group are only generate if an id attribute is present in initial schema, rules for prediction use predict/3 is target is double and classify/4 if it’
Example
Schema is: attribute(0,id, type(index)), attribute(1, width, type(double)). attribute(2, height, type(double)). attribute(3, class, type(categorical), [rectangle, square])]
Target is class
Rules generated:
ID
•	height(ID, Value) :- parallelogram(id(ID), height(Value), _ ,_ )
•	width(ID, Value) :- parallelogram(Id(ID), _,width(Value), _  )
•	class(ID, Value) :- parallelogram(Id(ID), _, _ ,class(Value))
Prediction
•	parallelogram(Id, Height, Width, Class) :- transform(parallelogram(Id, Height, Width, Class), TP, X), classify(X, N, [rectangle, square] , Label), Class = class(Label)
Error cases
•	Functor -> instantation_error, type_error(atom)
•	TP -> instantation_error, type_error(object_ref), instance_type_error(transform_process)
•	Network -> instantation_error, type_error(object_ref), instance_type_error(network)
**/
object Enhance : QuaternaryRelation.NonBacktrackable<ExecutionContext>("enhance") {

    override fun Solve.Request<ExecutionContext>.computeOne(
        first: Term,
        second: Term,
        third: Term,
        fourth: Term
    ): Solve.Response {
        ensuringArgumentIsInstantiated(0)
        ensuringArgumentIsInstantiated(1)
        ensuringArgumentIsInstantiated(2)
        ensuringArgumentIsInstantiated(3)
        ensuringArgumentIsAtom(0)
        ensuringArgumentIsTransformProcess(1)
        ensuringArgumentIsNetwork(2)
        ensuringArgumentIsAtom(3)
        val name = first.toString()
        val tp = (second as ObjectRef).`object` as LogicTransformProcess
        val network = (third as ObjectRef).`object` as LogicNetwork
        val target = fourth.toString()
        catchingMLRuntimeExceptions {
            val ruleList = RulesGenerator.enhance(name, tp, network, target)
            return replySuccess {
                addDynamicClauses(ruleList)
            }
        }
    }
}
