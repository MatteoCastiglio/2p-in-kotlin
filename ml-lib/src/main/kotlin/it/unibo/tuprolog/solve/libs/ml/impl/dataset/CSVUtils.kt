package it.unibo.tuprolog.solve.libs.ml.impl.dataset

import org.datavec.api.records.writer.RecordWriter
import org.datavec.api.records.writer.impl.csv.CSVRecordWriter
import org.datavec.api.split.FileSplit
import org.datavec.api.split.partition.NumberOfRecordsPartitioner
import org.datavec.api.writable.Writable
import java.io.File
import java.io.IOException

object CSVUtils {

    private fun createFile(fileName: String) {
        runCatching {
            val outputFile = File(fileName)
            if (outputFile.exists()) {
                outputFile.delete()
            }
            outputFile.createNewFile()
        }.onFailure {
            it is IOException || it is SecurityException
            throw DatasetException(it.message!!)
        }
    }

    fun createCSVWriter(fileName: String): RecordWriter {
        createFile(fileName)
        val rw = CSVRecordWriter()
        val p = NumberOfRecordsPartitioner()
        val inputSplit = FileSplit(File(fileName))
        rw.initialize(inputSplit, p)
        return rw
    }

    fun <R> writeToCSV(
        fileName: String,
        iterator: Iterator<R>,
        transform: (record: R) -> List<List<Writable>>
    ) {
        runCatching {
            val rw = createCSVWriter(fileName)
            while (iterator.hasNext()) {
                rw.writeBatch(transform(iterator.next()))
            }
            rw.close()
        }.onFailure {
            if (it is IOException) {
                throw DatasetException(it.message!!)
            }
        }
    }
}
