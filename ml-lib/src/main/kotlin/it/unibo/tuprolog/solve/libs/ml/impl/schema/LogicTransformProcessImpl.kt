package it.unibo.tuprolog.solve.libs.ml.impl.schema

import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.solve.libs.ml.LogicDataSet
import it.unibo.tuprolog.solve.libs.ml.LogicSchema
import it.unibo.tuprolog.solve.libs.ml.LogicTransformProcess
import it.unibo.tuprolog.solve.libs.ml.impl.dataset.CSVUtils
import it.unibo.tuprolog.solve.libs.ml.impl.dataset.DatasetBatchIterator
import it.unibo.tuprolog.solve.libs.ml.impl.dataset.LogicDataSetImpl
import it.unibo.tuprolog.solve.libs.ml.impl.dataset.RecordConverter
import org.datavec.api.transform.analysis.DataAnalysis
import org.datavec.api.writable.Writable
import org.datavec.local.transforms.AnalyzeLocal
import org.datavec.local.transforms.LocalTransformExecutor
import org.datavec.api.transform.TransformProcess as DL4JTransformProcess

class LogicTransformProcessImpl internal constructor(
    private val initial: LogicSchemaImpl,
    private val final: LogicSchemaImpl,
    private val operations: List<PipelineElement> = listOf(),
    private var needFitting: Boolean,
    private var deletedColumns: List<String> = listOf()
) :
    LogicTransformProcess {

    private var dataAnalysis: DataAnalysis = Unfitted
    private var transformer: DL4JTransformProcess? = null
    private var initialized = false

    fun plus(
        operation: PipelineElement,
        final: LogicSchemaImpl,
        deletedColumns: List<String>
    ): LogicTransformProcessImpl {
        return LogicTransformProcessImpl(initial, final, operations.plus(operation), needFitting, deletedColumns)
    }

    fun fit(dataIn: LogicDataSetImpl) {
        try {
            dataIn.reset()
            val recordReader = dataIn.recordReader
            dataAnalysis = AnalyzeLocal.analyze(initial.schema, recordReader)
            needFitting = false
        } catch (e: Exception) {
            throw TransformProcessException("The dataset doesn't match the schema")
        }
    }

    override val initialSchema: LogicSchema
        get() = initial

    override val finalSchema: LogicSchema
        get() = final

    override fun fit(dataIn: LogicDataSet) =
        fit(dataIn as LogicDataSetImpl)

    private fun initialize() {
        if (!initialized) {
            val builder = DL4JTransformProcess.Builder(initial.schema)
            operations.forEach { it.operation(builder, dataAnalysis) }
            transformer = builder.build()
            initialized = true
        }
    }

    private fun handleVarTerms(input: Struct): Struct {
        val indexDeleted = mutableListOf<Int>()
        (initial.attributes() zip input.args).forEachIndexed { count, (att, value) ->
            if ((value.isVariable || (value is Struct) && value.arity> 0 && value.args[0].isVariable)) {
                if (deletedColumns.contains(att.name)) {
                    indexDeleted.add(count)
                } else {
                    throw StopQueryException
                }
            }
        }
        val args = (initial.attributes() zip input.args).mapIndexed { count, (att, value) ->
            if (indexDeleted.contains(count)) {
                Struct.of(att.name, att.default)
            } else {
                value
            }
        }
        return Struct.of(input.functor, args)
    }

    private fun checkFitting() {
        if (needFitting) {
            throw(TransformProcessException("fit is needed for this process"))
        }
    }

    override fun transform(input: Struct): Struct {
        checkFitting()
        val inputWithNoVars = handleVarTerms(input)
        initialize()
        val functor = input.functor
        val data = mutableListOf(RecordConverter.recordFromStructWithSchema(inputWithNoVars, initialSchema))
        return try {
            val processedData = LocalTransformExecutor.execute(data, transformer)
            RecordConverter.structFromRecord(processedData[0], functor, final)
        } catch (e: Exception) {
            throw(TransformProcessException(e.message!!))
        }
    }

    private fun transformRecord(originalData: List<List<Writable>>): List<List<Writable>> {
        return LocalTransformExecutor.execute(originalData, transformer)
    }

    private fun transformDataset(dataIn: LogicDataSetImpl, fileName: String): LogicDataSetImpl {
        checkFitting()
        initialize()
        dataIn.reset()
        try {
            CSVUtils.writeToCSV(fileName, DatasetBatchIterator(batchSize, dataIn), ::transformRecord)
            return LogicDataSetImpl.fromCsv(fileName)
        } catch (e: Exception) {
            throw TransformProcessException("The dataset doesn't match the schema")
        }
    }

    override fun transformDataset(dataIn: LogicDataSet, fileName: String): LogicDataSetImpl =
        transformDataset(dataIn as LogicDataSetImpl, fileName)

    companion object {

        const val batchSize = 32

        private fun fromSchema(schema: LogicSchemaImpl): LogicTransformProcessImpl {
            return schema.transformProcess
        }

        fun fromSchema(schema: LogicSchema) =
            fromSchema(schema as LogicSchemaImpl)

        private fun schemaOperation(
            operation: PipelineElement,
            initial: LogicSchemaImpl,
            columns: List<String>,
            requiresFit: Boolean = false,
            delete: Boolean = false
        ): LogicSchemaImpl =
            if (!initial.attributesNames().containsAll(columns)) {
                throw TransformProcessException("Invalid columns names: $columns not in ${initial.attributesNames()}")
            } else {
                val d4jSchema = initial.schema
                val transformedSchema = operation.getTransformedSchema(d4jSchema)
                val final = LogicSchemaImpl(transformedSchema)
                val oldTp = initial.transformProcess
                val newTp = oldTp.plus(operation, final, deletedColumns = oldTp.deletedColumns)
                if (requiresFit) {
                    newTp.needFitting = true
                }
                if (delete) {
                    newTp.deletedColumns = newTp.deletedColumns.plus(columns)
                }
                final.transformProcess = newTp
                final
            }

        private fun normalize(schema: LogicSchemaImpl, columns: List<String>): LogicSchemaImpl {
            val operation = Normalization(columns)
            return schemaOperation(operation, schema, columns, true)
        }

        fun normalize(schema: LogicSchema, columns: List<String>): LogicSchemaImpl =
            normalize(schema as LogicSchemaImpl, columns)

        private fun oneHotEncoding(schema: LogicSchemaImpl, columns: List<String>): LogicSchemaImpl {
            val operation = OneHotEncoding(columns)
            return schemaOperation(operation, schema, columns)
        }

        fun oneHotEncoding(schema: LogicSchema, columns: List<String>): LogicSchemaImpl =
            oneHotEncoding(schema as LogicSchemaImpl, columns)

        private fun deleteColumns(schema: LogicSchemaImpl, columns: List<String>): LogicSchemaImpl {
            val operation = DeleteColumns(columns)
            return schemaOperation(operation, schema, columns, delete = true)
        }

        fun deleteColumns(schema: LogicSchema, columns: List<String>): LogicSchemaImpl =
            deleteColumns(schema as LogicSchemaImpl, columns)
    }
}
