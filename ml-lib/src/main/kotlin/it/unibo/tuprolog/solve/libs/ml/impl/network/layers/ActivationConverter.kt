package it.unibo.tuprolog.solve.libs.ml.impl.network.layers

import it.unibo.tuprolog.solve.libs.ml.LogicActivation
import org.nd4j.linalg.activations.Activation
import org.nd4j.linalg.activations.IActivation
import org.nd4j.linalg.activations.impl.ActivationIdentity
import org.nd4j.linalg.activations.impl.ActivationReLU
import org.nd4j.linalg.activations.impl.ActivationSigmoid
import org.nd4j.linalg.activations.impl.ActivationSoftmax
import org.nd4j.linalg.activations.impl.ActivationTanH

object ActivationConverter {

    fun implToLogic(dl4jActivation: IActivation): LogicActivation {
        return when (dl4jActivation) {
            is ActivationReLU -> LogicActivation.RELU
            is ActivationTanH -> LogicActivation.TANH
            is ActivationSigmoid -> LogicActivation.SIGMOID
            is ActivationSoftmax -> LogicActivation.SOFTMAX
            is ActivationIdentity -> LogicActivation.IDENTITY
            else -> LogicActivation.OTHER
        }
    }

    fun logicToImpl(activation: LogicActivation): Activation =
        Activation.valueOf(activation.toString().toUpperCase())
}
