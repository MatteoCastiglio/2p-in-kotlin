package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicSchema
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.solve.primitive.TernaryRelation
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith
import it.unibo.tuprolog.core.List as LogicList

abstract class SchemaOperation(functor: String) : TernaryRelation.Functional<ExecutionContext>(functor) {

    abstract fun getTransformedSchema(schema: LogicSchema, columns: List<String>): LogicSchema

    override fun Solve.Request<ExecutionContext>.computeOneSubstitution(
        first: Term,
        second: Term,
        third: Term
    ): Substitution {
        ensuringArgumentIsVar(2)
        ensuringArgumentIsInstantiated(0)
        ensuringArgumentIsInstantiatedList(1)
        ensuringArgumentIsSchema(0)
        ensuringArgumentIsAtomList(1)
        val columns = (second as LogicList).toList().map { it.toString() }
        val schemaInRef = (first as ObjectRef).`object` as LogicSchema
        return catchingMLRuntimeExceptions {
            val schemaOut = getTransformedSchema(schemaInRef, columns)
            val schemaOutRef = ObjectRef.of(schemaOut)
            schemaOutRef mguWith third
        }
    }
}
