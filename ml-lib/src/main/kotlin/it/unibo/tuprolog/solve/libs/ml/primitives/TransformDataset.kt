package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicDataSet
import it.unibo.tuprolog.solve.libs.ml.LogicTransformProcess
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.QuaternaryRelation
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith

/**
transform_dataset/4
Description = transform_dataset(DataIn, TP, Filename, DataOut)
transform_dataset(+dataset, +transform_process, +atom , -dataset)
The result is the creation of a Dataset D, in which every record is the correspondent record of DataIn transformed according to steps defined in TP. The result is the substitution {D/DataOut}.
Side Effects
The dataset is physically written in csv format in a file located at Filename
Error cases
•	DataIn -> instantation_error, type_error(object_ref), instance_type_error(dataset)
•	TP -> instantation_error, type_error(object_ref), instance_type_error(transform_process)
•	Filename -> instantation_error, type_error(atom)
•	DataOut -> type_error(var)
•	Runtime errors-> runtime_error
**/
object TransformDataset : QuaternaryRelation.Functional<ExecutionContext>("transform_dataset") {

    override fun Solve.Request<ExecutionContext>.computeOneSubstitution(
        first: Term,
        second: Term,
        third: Term,
        fourth: Term
    ): Substitution {
        ensuringArgumentIsInstantiated(0)
        ensuringArgumentIsInstantiated(1)
        ensuringArgumentIsInstantiated(2)
        ensuringArgumentIsDataset(0)
        ensuringArgumentIsTransformProcess(1)
        ensuringArgumentIsAtom(2)
        ensuringArgumentIsVar(3)
        val dataset = (first as ObjectRef).`object` as LogicDataSet
        val tp = (second as ObjectRef).`object` as LogicTransformProcess
        val name = (third as Atom).toString().replace("'", "")
        return catchingMLRuntimeExceptions {
            val result = tp.transformDataset(dataset, name)
            fourth mguWith ObjectRef.of(result)
        }
    }
}
