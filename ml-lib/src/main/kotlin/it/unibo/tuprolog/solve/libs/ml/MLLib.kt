package it.unibo.tuprolog.solve.libs.ml

import it.unibo.tuprolog.solve.library.AliasedLibrary
import it.unibo.tuprolog.solve.library.Library
import it.unibo.tuprolog.solve.libs.ml.primitives.Accuracy
import it.unibo.tuprolog.solve.libs.ml.primitives.Classify
import it.unibo.tuprolog.solve.libs.ml.primitives.DeleteColumns
import it.unibo.tuprolog.solve.libs.ml.primitives.DenseLayer
import it.unibo.tuprolog.solve.libs.ml.primitives.Enhance
import it.unibo.tuprolog.solve.libs.ml.primitives.Fit
import it.unibo.tuprolog.solve.libs.ml.primitives.InputLayer
import it.unibo.tuprolog.solve.libs.ml.primitives.LoadDataSetFromCSV
import it.unibo.tuprolog.solve.libs.ml.primitives.LoadDataSetFromTheory
import it.unibo.tuprolog.solve.libs.ml.primitives.MSE
import it.unibo.tuprolog.solve.libs.ml.primitives.Network
import it.unibo.tuprolog.solve.libs.ml.primitives.Normalize
import it.unibo.tuprolog.solve.libs.ml.primitives.OneHotEncoding
import it.unibo.tuprolog.solve.libs.ml.primitives.OutputLayer
import it.unibo.tuprolog.solve.libs.ml.primitives.Predict
import it.unibo.tuprolog.solve.libs.ml.primitives.PredictDataset
import it.unibo.tuprolog.solve.libs.ml.primitives.RandomSplit
import it.unibo.tuprolog.solve.libs.ml.primitives.SchemaAttributes
import it.unibo.tuprolog.solve.libs.ml.primitives.SchemaFromTheory
import it.unibo.tuprolog.solve.libs.ml.primitives.TheoryFromDataset
import it.unibo.tuprolog.solve.libs.ml.primitives.Train
import it.unibo.tuprolog.solve.libs.ml.primitives.Train4
import it.unibo.tuprolog.solve.libs.ml.primitives.Transform
import it.unibo.tuprolog.solve.libs.ml.primitives.TransformDataset
import it.unibo.tuprolog.solve.libs.ml.primitives.TransformProcess
import it.unibo.tuprolog.solve.primitive.PrimitiveWrapper
import it.unibo.tuprolog.theory.Theory

/**
@author Matteo
**/

object MLLib : AliasedLibrary by
    Library.aliased(
        theory = Theory.indexedOf(
            sequenceOf(
                Train4
            ).map { it.wrappedImplementation }
        ),
        primitives = sequenceOf<PrimitiveWrapper<*>>(
            InputLayer,
            DenseLayer,
            OutputLayer,
            Network,
            LoadDataSetFromCSV,
            LoadDataSetFromTheory,
            TheoryFromDataset,
            RandomSplit,
            SchemaFromTheory,
            SchemaAttributes,
            Normalize,
            OneHotEncoding,
            DeleteColumns,
            TransformProcess,
            Fit,
            Transform,
            TransformDataset,
            Predict,
            Classify,
            PredictDataset,
            Train,
            Accuracy,
            MSE,
            Enhance
        ).map { it.descriptionPair }.toMap(),
        alias = "prolog.ml"
    )
