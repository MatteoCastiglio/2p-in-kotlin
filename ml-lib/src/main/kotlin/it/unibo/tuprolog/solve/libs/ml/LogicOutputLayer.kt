package it.unibo.tuprolog.solve.libs.ml

interface LogicOutputLayer : LogicDenseLayer {

    val loss: LogicLoss
}
