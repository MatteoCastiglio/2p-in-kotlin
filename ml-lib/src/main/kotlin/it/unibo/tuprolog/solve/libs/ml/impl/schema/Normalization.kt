package it.unibo.tuprolog.solve.libs.ml.impl.schema

import org.datavec.api.transform.TransformProcess
import org.datavec.api.transform.analysis.DataAnalysis
import org.datavec.api.transform.schema.Schema
import org.datavec.api.transform.transform.normalize.Normalize

class Normalization(private val columns: List<String>) : PipelineElement {

    override fun getTransformedSchema(schema: Schema): Schema {
        return schema
    }

    override fun operation(builder: TransformProcess.Builder, da: DataAnalysis) {
        columns.forEach { builder.normalize(it, Normalize.Standardize, da) }
    }
}
