package it.unibo.tuprolog.solve.libs.ml.exception

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.core.TermFormatter
import it.unibo.tuprolog.core.ToTermConvertible
import it.unibo.tuprolog.core.format
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.Signature
import it.unibo.tuprolog.solve.exception.PrologError

class DomainErrorML(
    message: String? = null,
    cause: Throwable? = null,
    contexts: Array<ExecutionContext>,
    private val expectedDomain: Expected,
    private val actualValue: Term,
    extraData: Term? = null
) : PrologError(message, cause, contexts, Atom.of(typeFunctor), extraData) {

    constructor(
        message: String? = null,
        cause: Throwable? = null,
        context: ExecutionContext,
        expectedDomain: Expected,
        actualValue: Term,
        extraData: Term? = null
    ) : this(message, cause, arrayOf(context), expectedDomain, actualValue, extraData)

    override fun updateContext(newContext: ExecutionContext): DomainErrorML =
        DomainErrorML(message, cause, contexts.setFirst(newContext), expectedDomain, actualValue, extraData)

    override fun pushContext(newContext: ExecutionContext): DomainErrorML =
        DomainErrorML(message, cause, contexts.addLast(newContext), expectedDomain, actualValue, extraData)

    override val type: Struct by lazy { Struct.of(super.type.functor, expectedDomain.toTerm(), actualValue) }

    companion object {

/*
        fun of(
            context: ExecutionContext,
            expectedDomain: Expected,
            actualValueValue: Term,
            message: String
        ) = message(message) { m, extra ->
            DomainErrorDL(
                message = m,
                context = context,
                expectedDomain = expectedDomain,
                actualValue = actualValueValue,
                extraData = extra
            )
        }

        fun forArgumentList(
            context: ExecutionContext,
            procedure: Signature,
            expectedDomain: Expected,
            actualValue: Term,
            index: Int? = null
        ) = message(
            (index?.let { "The $it-th argument" } ?: "An argument") +
                " of `${procedure.pretty()}` should be a list of `$expectedDomain`, " +
                "but `${actualValue.pretty()}` has been provided instead"
        ) { m, extra ->
            DomainErrorDL(
                message = m,
                context = context,
                expectedDomain = expectedDomain,
                actualValue = actualValue,
                extraData = extra
            )
        }

 */

        fun forArgument(
            context: ExecutionContext,
            procedure: Signature,
            expectedDomain: Expected,
            actualValue: Term,
            index: Int? = null
        ): DomainErrorML {
            val msg = (index?.let { "The $it-th argument" } ?: "An argument") +
                " of `${procedure.toIndicator()}` should be a `$expectedDomain`, " +
                "but `${actualValue.format(TermFormatter.prettyVariables())}` has been provided instead"
            return DomainErrorML(message = msg, context = context, expectedDomain = expectedDomain, actualValue = actualValue, extraData = Atom.of(msg))
        }

/*
        fun forGoal(
            context: ExecutionContext,
            procedure: Signature,
            expectedDomain: Expected,
            actualValue: Term
        ) = message(
            "Subgoal `${actualValue.pretty()}` of ${procedure.pretty()} is not a $expectedDomain term"
        ) { m, extra ->
            DomainErrorDL(
                message = m,
                context = context,
                expectedDomain = expectedDomain,
                actualValue = actualValue,
                extraData = extra
            )
        }
*/
        /** The type error Struct functor */
        const val typeFunctor = "domain_error"
    }

    /**
     * A class describing the expected type whose absence caused the error
     */
    enum class Expected : ToTermConvertible {
        ACTIVATION,
        LOSS,
        ATTRIBUTE;

        /**
         * The type expected string description
         */
        private val type: String by lazy { name.toLowerCase() }

        /** A function to transform the type to corresponding [Atom] representation */
        override fun toTerm(): Atom = Atom.of(type)

        override fun toString(): String = type

        companion object {
            /** Returns the Expected instance described by [type]; creates a new instance only if [type] was not predefined */

            fun of(type: String): Expected = valueOf(type.toUpperCase())

            /** Gets [Expected] instance from [term] representation, if possible */

            fun fromTerm(term: Term): Expected? = when (term) {
                is Atom -> of(term.value)
                else -> null
            }
        }
    }
}
