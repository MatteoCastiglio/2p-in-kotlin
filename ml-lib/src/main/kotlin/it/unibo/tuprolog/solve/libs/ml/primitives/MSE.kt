package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Numeric
import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicNetwork
import it.unibo.tuprolog.solve.libs.ml.impl.dataset.LogicDataSetImpl
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.QuaternaryRelation
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith

/**
mse/5
Description = mse(Network, Dataset, Index, MSE)
mse(+network, +dataset, +integer, +integer, -integer )
Network is evaluated by computing mean squared error E on the whole dataset, Index is used to define which part of the dataset are used as target. The substitution {E/MSE} is returned
Error cases
Network -> instantation_error, type_error(object_ref), instance_type_error(nework)
Dataset -> instantation_error, type_error(object_ref), instance_type_error(dataset)
Index -> instantation_error, type_error(integer)
MSE-> type_error(var)
Runtime errors -> runtime_error
**/
object MSE : QuaternaryRelation.Functional<ExecutionContext>("mse") {

    override fun Solve.Request<ExecutionContext>.computeOneSubstitution(
        first: Term,
        second: Term,
        third: Term,
        fourth: Term
    ): Substitution {
        ensuringArgumentIsInstantiated(0)
        ensuringArgumentIsInstantiated(1)
        ensuringArgumentIsInstantiated(2)
        ensuringArgumentIsNetwork(0)
        ensuringArgumentIsDataset(1)
        ensuringArgumentIsInteger(2)
        ensuringArgumentIsVar(3)
        val network = (first as ObjectRef).`object` as LogicNetwork
        val dataset = (second as ObjectRef).`object` as LogicDataSetImpl
        val labelIndex = (third as Numeric).intValue.toInt()
        return catchingMLRuntimeExceptions {
            val result = network.mse(dataset, labelIndex)
            fourth mguWith Numeric.of(result)
        }
    }
}
