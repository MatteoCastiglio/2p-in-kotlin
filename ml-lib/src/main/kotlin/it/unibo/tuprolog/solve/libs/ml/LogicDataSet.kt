package it.unibo.tuprolog.solve.libs.ml

import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.solve.libs.ml.impl.dataset.LogicDataSetImpl

interface LogicDataSet {

    fun theory(name: String, schema: LogicSchema): Sequence<Struct>
    fun hasNext(): Boolean
    fun split(ratio: Double): Pair<LogicDataSet, LogicDataSet>

    companion object {

        fun fromCsv(fileName: String): LogicDataSetImpl =
            LogicDataSetImpl.fromCsv(fileName)

        fun fromStruct(struct: Struct): LogicDataSetImpl =
            LogicDataSetImpl.fromStruct(struct)

        fun fromTheory(theory: Iterable<Struct>): LogicDataSetImpl =
            LogicDataSetImpl.fromTheory(theory)
    }
}
