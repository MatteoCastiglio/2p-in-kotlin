package it.unibo.tuprolog.solve.libs.ml

interface LogicLayer {
    val size: Long
}
