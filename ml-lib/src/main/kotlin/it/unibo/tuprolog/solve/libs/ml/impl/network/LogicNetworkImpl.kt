package it.unibo.tuprolog.solve.libs.ml.impl.network

import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.solve.libs.ml.LogicDataSet
import it.unibo.tuprolog.solve.libs.ml.LogicLayer
import it.unibo.tuprolog.solve.libs.ml.LogicNetwork
import it.unibo.tuprolog.solve.libs.ml.LogicOutputLayer
import it.unibo.tuprolog.solve.libs.ml.SchemaDefinitionException
import it.unibo.tuprolog.solve.libs.ml.impl.dataset.CSVUtils
import it.unibo.tuprolog.solve.libs.ml.impl.dataset.LogicDataSetImpl
import it.unibo.tuprolog.solve.libs.ml.impl.dataset.RecordConverter
import it.unibo.tuprolog.solve.libs.ml.impl.network.layers.LogicDenseLayerImpl
import it.unibo.tuprolog.solve.libs.ml.impl.schema.StopQueryException
import org.datavec.api.writable.Writable
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator
import org.deeplearning4j.exception.DL4JInvalidConfigException
import org.deeplearning4j.nn.conf.NeuralNetConfiguration
import org.deeplearning4j.nn.conf.layers.Layer
import org.nd4j.evaluation.classification.Evaluation
import org.nd4j.evaluation.regression.RegressionEvaluation
import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.dataset.DataSet
import org.nd4j.linalg.factory.Nd4j
import org.nd4j.linalg.learning.config.Adam
import org.datavec.api.util.ndarray.RecordConverter as Nd4jRecordConverter
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork as DL4JMultiLayerNetwork

class LogicNetworkImpl private constructor(
    private val networkImpl: DL4JMultiLayerNetwork,
    override val outputLayer: LogicOutputLayer
) :
    LogicNetwork {

    private fun getVectorFromStruct(input: Struct): INDArray {
        input.args.forEach {
            if (it.isVariable || it.isStruct && (it as Struct).args[0].isVariable) {
                throw StopQueryException
            }
        }
        return Nd4jRecordConverter.toArray(RecordConverter.recordFromStruct(input))
    }

    override fun classify(input: Struct, classList: List<String>): String {
        return try {
            if (classList.size.toLong() != outputLayer.size) {
                throw SchemaDefinitionException("number of label not equal to number of output neurons ")
            }
            val data = getVectorFromStruct(input)
            val dataset = DataSet(data, Nd4j.create(classList.size))
            dataset.labelNames = classList
            networkImpl.predict(dataset).toList()[0]
        } catch (e: Exception) {
            when (e) {
                is StopQueryException -> throw e
                else -> throw NetworkException(e.message!!)
            }
        }
    }

    override fun predict(input: Struct): List<Double> {
        try {
            val data = getVectorFromStruct(input)
            val output = networkImpl.output(data)
            return if (output.isScalar) {
                listOf(output.getDouble(0))
            } else {
                val size = output.size(0)
                return (0..size + 1).map { output.getDouble(it) }
            }
        } catch (e: Exception) {
            when (e) {
                is StopQueryException -> throw e
                else -> throw NetworkException(e.message!!)
            }
        }
    }

    private fun predictRecord(r: DataSet): List<List<Writable>> {
        val input = r.features
        val output = networkImpl.output(input)
        return Nd4jRecordConverter.toRecords(Nd4j.hstack(input, output))
    }

    private fun train(dataset: LogicDataSetImpl, labelIndexFrom: Int, labelIndexTo: Int, iterations: Int) {
        dataset.reset()
        try {
            val iterator =
                RecordReaderDataSetIterator(dataset.recordReader, batchSize, labelIndexFrom, labelIndexTo, true)
            networkImpl.fit(iterator, iterations)
        } catch (e: Exception) {
            throw NetworkException(e.message!!)
        }
    }

    override fun train(dataset: LogicDataSet, labelIndexFrom: Int, labelIndexTo: Int, iterations: Int) =
        train(dataset as LogicDataSetImpl, labelIndexFrom, labelIndexTo, iterations)

    private fun accuracy(dataset: LogicDataSetImpl, labelIndexFrom: Int, labelIndexTo: Int): Double {
        dataset.reset()
        try {
            val iterator =
                RecordReaderDataSetIterator(dataset.recordReader, batchSize, labelIndexFrom, labelIndexTo, true)
            val eval = networkImpl.evaluate<Evaluation>(iterator)
            return eval.accuracy()
        } catch (e: Exception) {
            throw NetworkException(e.message!!)
        }
    }

    override fun accuracy(dataset: LogicDataSet, labelIndexFrom: Int, labelIndexTo: Int): Double =
        accuracy(dataset as LogicDataSetImpl, labelIndexFrom, labelIndexTo)

    private fun mse(dataset: LogicDataSetImpl, labelIndex: Int): Double {
        dataset.reset()
        try {
            val iterator = RecordReaderDataSetIterator(dataset.recordReader, batchSize, labelIndex, labelIndex, true)
            val eval = networkImpl.evaluateRegression<RegressionEvaluation>(iterator)
            return eval.averageMeanSquaredError()
        } catch (e: Exception) {
            throw NetworkException(e.message!!)
        }
    }

    override fun mse(dataset: LogicDataSet, labelIndex: Int): Double =
        mse(dataset as LogicDataSetImpl, labelIndex)

    private fun predictDataset(data: LogicDataSetImpl, fileName: String): LogicDataSetImpl {
        data.reset()
        val recordReader = data.recordReader
        val iterator = RecordReaderDataSetIterator(recordReader, batchSize)
        CSVUtils.writeToCSV(fileName, iterator, ::predictRecord)
        return LogicDataSetImpl.fromCsv(fileName)
    }

    override fun predictDataset(data: LogicDataSet, fileName: String): LogicDataSetImpl =
        predictDataset(data as LogicDataSetImpl, fileName)

    override fun summary(): String {
        return networkImpl.summary()
    }

    companion object {

        const val batchSize = 32

        fun of(outputLayer: LogicOutputLayer): LogicNetworkImpl {
            var currentLayer = outputLayer as LogicLayer
            val layerList: MutableList<Layer> = mutableListOf()
            while (currentLayer is LogicDenseLayerImpl) {
                layerList.add(currentLayer.layerImpl as Layer)
                currentLayer = currentLayer.previousLayer
            }
            layerList.reverse()
            val networkConf = NeuralNetConfiguration.Builder()
                .updater(Adam())
                .l2(1e-2).list()
            layerList.forEach { networkConf.layer(it) }
            try {
                val conf = networkConf.build()
                val network = (org.deeplearning4j.nn.multilayer.MultiLayerNetwork(conf))
                network.init()
                return LogicNetworkImpl(network, outputLayer)
            } catch (e: DL4JInvalidConfigException) {
                throw NetworkException(e.message!!)
            }
        }
    }
}
