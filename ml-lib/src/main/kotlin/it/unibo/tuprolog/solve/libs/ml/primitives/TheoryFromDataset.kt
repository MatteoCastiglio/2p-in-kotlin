package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.core.Var
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicDataSet
import it.unibo.tuprolog.solve.libs.ml.LogicSchema
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.QuaternaryRelation
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith

/**
theory_from_dataset/4
Description = theory_from_dataset(Functor, Schema, Dataset, Record)
theory_from_dataset (+atom, +schema, +dataset, ?struct)
The primitive returns a sequence of substitutions {Ri/Record}, i from 0 to N record in Dataset, where Ri is the translation of i-th row of the dataset translated according of Schema.
Error cases
•	Functor -> instantation_error, type_error(atom)
•	Schema -> instantation_error, type_error(object_ref), instance_type_error(schema)
•	Dataset -> instantation_error, type_error(object_ref), instance_type_error(dataset)
•	Runtime errors-> runtime
**/
object TheoryFromDataset : QuaternaryRelation.WithoutSideEffects<ExecutionContext>("theory_from_dataset") {

    override fun Solve.Request<ExecutionContext>.computeAllSubstitutions(
        first: Term,
        second: Term,
        third: Term,
        fourth: Term
    ): Sequence<Substitution> {
        ensuringArgumentIsInstantiated(0)
        ensuringArgumentIsInstantiated(1)
        ensuringArgumentIsInstantiated(2)
        ensuringArgumentIsAtom(0)
        ensuringArgumentIsSchema(1)
        ensuringArgumentIsDataset(2)
        val functor = first.toString()
        val schema = (second as ObjectRef).`object` as LogicSchema
        val dataset = (third as ObjectRef).`object` as LogicDataSet
        return catchingMLRuntimeExceptions {
            when (fourth) {
                is Var, is Struct -> {
                    dataset.theory(functor, schema).map { (fourth mguWith it) }
                }
                else -> sequenceOf(Substitution.failed())
            }
        }
    }
}
