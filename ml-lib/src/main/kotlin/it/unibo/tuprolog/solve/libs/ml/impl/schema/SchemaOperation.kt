package it.unibo.tuprolog.solve.libs.ml.impl.schema

import org.datavec.api.transform.TransformProcess
import org.datavec.api.transform.schema.Schema

abstract class SchemaOperation : PipelineElement {

    override fun getTransformedSchema(schema: Schema): Schema {
        try {
            val builder = TransformProcess.Builder(schema)
            operation(builder, Unfitted)
            val transformer = builder.build()
            return transformer.finalSchema
        } catch (e: Exception) {
            throw(TransformProcessException(e.message!!))
        }
    }
}
