package it.unibo.tuprolog.solve.libs.ml.impl.schema

class TransformProcessException(s: String) : Exception(s)
