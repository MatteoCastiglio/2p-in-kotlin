package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicDataSet
import it.unibo.tuprolog.solve.libs.ml.LogicNetwork
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.QuaternaryRelation
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith

/**
predict_dataset/4
Description = predict_dataset(DataIn, Network, Filename, DataOut )
predict_dataset(+dataset, +network, +atom , -dataset)
The result is the creation of a Dataset D, in which every record is the correspondent record of DataIn with the addition of numerical value which are outputs of Network for that given record and the substitution {D/DataOut}
Side Effects
The dataset is physically written in csv format in a file located at Filename
Error cases
DataIn -> instantation_error, type_error(object_ref), instance_type_error(dataset)
Network -> instantation_error, type_error(object_ref), instance_type_error(nework)
Filename -> instantation_error, type_error(atom)
DataOut -> type_error(var)
Runtime errors->  runtime_error
**/
object PredictDataset : QuaternaryRelation.Functional<ExecutionContext>("predict_dataset") {

    override fun Solve.Request<ExecutionContext>.computeOneSubstitution(
        first: Term,
        second: Term,
        third: Term,
        fourth: Term
    ): Substitution {
        ensuringArgumentIsInstantiated(0)
        ensuringArgumentIsInstantiated(1)
        ensuringArgumentIsInstantiated(2)
        ensuringArgumentIsDataset(0)
        ensuringArgumentIsNetwork(1)
        ensuringArgumentIsAtom(2)
        ensuringArgumentIsVar(3)
        val dataset = (first as ObjectRef).`object` as LogicDataSet
        val network = (second as ObjectRef).`object` as LogicNetwork
        val name = (third as Atom).toString().replace("'", "")
        return catchingMLRuntimeExceptions {
            val result = network.predictDataset(dataset, name)
            fourth mguWith ObjectRef.of(result)
        }
    }
}
