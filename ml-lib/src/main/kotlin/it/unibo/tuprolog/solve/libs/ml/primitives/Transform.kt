package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicTransformProcess
import it.unibo.tuprolog.solve.libs.ml.impl.schema.StopQueryException
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.solve.primitive.TernaryRelation
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith

/**
transform/3
Description = transform(Input , TP, Output)
transform(+struct, +TP, -struct)
Input is transformed according to TP in a struct. Output matches final schema of TP and has the same functor of Input. The substitution {O/Output} is returned.
Error cases
•	Input -> instantation_error, runtime_error*
•	TP-> instantation_error, type_error(object_ref), instance_type_error(transform_process)
•	Output -> type_error(var)
•	Runtime errors -> runtime_error
* Input must match initial schema of TP (see conversion rules).
**/
object Transform : TernaryRelation.Functional<ExecutionContext>("transform") {

    override fun Solve.Request<ExecutionContext>.computeOneSubstitution(
        first: Term,
        second: Term,
        third: Term
    ): Substitution {
        ensuringArgumentIsInstantiated(0)
        ensuringArgumentIsInstantiated(1)
        ensuringArgumentIsTransformProcess(1)
        val tp = (second as ObjectRef).`object` as LogicTransformProcess
        return try {
            catchingMLRuntimeExceptions {
                val result = tp.transform(first as Struct)
                third mguWith result
            }
        } catch (e: StopQueryException) {
            Substitution.failed()
        }
    }
}
