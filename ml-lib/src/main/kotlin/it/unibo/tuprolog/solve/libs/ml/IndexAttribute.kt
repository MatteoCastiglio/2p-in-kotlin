package it.unibo.tuprolog.solve.libs.ml

import it.unibo.tuprolog.core.Numeric
import it.unibo.tuprolog.core.Term

data class IndexAttribute(override val index: Int, override val name: String) : Attribute {

    override val type: String
        get() = "Index"

    override val default: Term
        get() = Numeric.of(0)
}
