package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.core.Var
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.exception.error.InstantiationError
import it.unibo.tuprolog.solve.exception.error.TypeError
import it.unibo.tuprolog.solve.libs.ml.LogicActivation
import it.unibo.tuprolog.solve.libs.ml.LogicDataSet
import it.unibo.tuprolog.solve.libs.ml.LogicDenseLayer
import it.unibo.tuprolog.solve.libs.ml.LogicInputLayer
import it.unibo.tuprolog.solve.libs.ml.LogicLoss
import it.unibo.tuprolog.solve.libs.ml.LogicNetwork
import it.unibo.tuprolog.solve.libs.ml.LogicOutputLayer
import it.unibo.tuprolog.solve.libs.ml.LogicSchema
import it.unibo.tuprolog.solve.libs.ml.LogicTransformProcess
import it.unibo.tuprolog.solve.libs.ml.SchemaDefinitionException
import it.unibo.tuprolog.solve.libs.ml.exception.DomainErrorML
import it.unibo.tuprolog.solve.libs.ml.exception.InstanceTypeError
import it.unibo.tuprolog.solve.libs.ml.exception.RuntimeError
import it.unibo.tuprolog.solve.libs.ml.impl.dataset.DatasetException
import it.unibo.tuprolog.solve.libs.ml.impl.network.NetworkException
import it.unibo.tuprolog.solve.libs.ml.impl.schema.TransformProcessException
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.libs.oop.primitives.ensuringArgumentIsObjectRef
import it.unibo.tuprolog.solve.primitive.PrimitiveWrapper.Companion.ensuringArgumentIsInstantiated
import it.unibo.tuprolog.solve.primitive.PrimitiveWrapper.Companion.ensuringArgumentIsList
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.core.List as LogicList

fun <C : ExecutionContext> Solve.Request<C>.ensuringArgumentIsLayer(index: Int): Solve.Request<C> {
    ensuringArgumentIsObjectRef(index)
    val arg = arguments[index] as ObjectRef
    if (arg.`object` !is LogicInputLayer && arg.`object` !is LogicDenseLayer) {
        throw InstanceTypeError.forArgument(context, signature, InstanceTypeError.Expected.LAYER, arg, index)
    }
    return this
}

fun <C : ExecutionContext> Solve.Request<C>.ensuringArgumentIsInputLayer(index: Int): Solve.Request<C> {
    ensuringArgumentIsObjectRef(index)
    val arg = arguments[index] as ObjectRef
    if (arg.`object` !is LogicInputLayer) {
        throw InstanceTypeError.forArgument(context, signature, InstanceTypeError.Expected.INPUT_LAYER, arg, index)
    }
    return this
}

fun <C : ExecutionContext> Solve.Request<C>.ensuringArgumentIsDenseLayer(index: Int): Solve.Request<C> {
    ensuringArgumentIsObjectRef(index)
    val arg = arguments[index] as ObjectRef
    if (arg.`object` !is LogicDenseLayer) {
        throw InstanceTypeError.forArgument(context, signature, InstanceTypeError.Expected.DENSE_LAYER, arg, index)
    }
    return this
}

fun <C : ExecutionContext> Solve.Request<C>.ensuringArgumentIsOutputLayer(index: Int): Solve.Request<C> {
    ensuringArgumentIsObjectRef(index)
    val arg = arguments[index] as ObjectRef
    if (arg.`object` !is LogicOutputLayer) {
        throw InstanceTypeError.forArgument(context, signature, InstanceTypeError.Expected.OUTPUT_LAYER, arg, index)
    }
    return this
}

fun <C : ExecutionContext> Solve.Request<C>.ensuringArgumentIsNetwork(index: Int): Solve.Request<C> {
    ensuringArgumentIsObjectRef(index)
    val arg = arguments[index] as ObjectRef
    if (arg.`object` !is LogicNetwork) {
        throw InstanceTypeError.forArgument(context, signature, InstanceTypeError.Expected.NETWORK, arg, index)
    }
    return this
}

fun <C : ExecutionContext> Solve.Request<C>.ensuringArgumentIsSchema(index: Int): Solve.Request<C> {
    ensuringArgumentIsObjectRef(index)
    val arg = arguments[index] as ObjectRef
    if (arg.`object` !is LogicSchema) {
        throw InstanceTypeError.forArgument(context, signature, InstanceTypeError.Expected.SCHEMA, arg, index)
    }
    return this
}

fun <C : ExecutionContext> Solve.Request<C>.ensuringArgumentIsDataset(index: Int): Solve.Request<C> {
    ensuringArgumentIsObjectRef(index)
    val arg = arguments[index] as ObjectRef
    if (arg.`object` !is LogicDataSet) {
        throw InstanceTypeError.forArgument(context, signature, InstanceTypeError.Expected.DATASET, arg, index)
    }
    return this
}

fun <C : ExecutionContext> Solve.Request<C>.ensuringArgumentIsTransformProcess(index: Int): Solve.Request<C> {
    ensuringArgumentIsObjectRef(index)
    val arg = arguments[index] as ObjectRef
    if (arg.`object` !is LogicTransformProcess) {
        throw InstanceTypeError.forArgument(context, signature, InstanceTypeError.Expected.TRANSFORM_PROCESS, arg, index)
    }
    return this
}

fun <C : ExecutionContext> Solve.Request<C>.ensuringArgumentIsActivation(index: Int): Solve.Request<C> {
    val arg = arguments[index]
    if (LogicActivation.fromStruct(arg as Struct) == null) {
        throw DomainErrorML.forArgument(context, signature, DomainErrorML.Expected.ACTIVATION, arg, index)
    }
    return this
}

fun <C : ExecutionContext> Solve.Request<C>.ensuringArgumentIsLoss(index: Int): Solve.Request<C> {
    val arg = arguments[index]
    if (LogicLoss.fromStruct(arg as Struct) == null) {
        throw DomainErrorML.forArgument(context, signature, DomainErrorML.Expected.LOSS, arg, index)
    }

    return this
}

fun <C : ExecutionContext> Solve.Request<C>.ensuringArgumentIsVar(index: Int): Solve.Request<C> =
    when (val arg = arguments[index]) {
        !is Var -> throw TypeError.forArgument(context, signature, TypeError.Expected.VAR, arg, index)
        else -> this
    }

fun <C : ExecutionContext> Solve.Request<C>.ensuringArgumentIsInstantiatedList(index: Int): Solve.Request<C> {
    val arg = arguments[index]
    ensuringArgumentIsInstantiated(index)
    ensuringArgumentIsList(index)
    (arg as LogicList).toSequence().forEach {
        if (it is Var) {
            throw InstantiationError.forArgument(
                context,
                signature,
                it,
                index
            )
        }
    }
    return this
}

fun <C : ExecutionContext> Solve.Request<C>.ensuringArgumentIsAtomList(index: Int): Solve.Request<C> {
    val arg = arguments[index]
    (arg as LogicList).toSequence().forEach {
        if (it !is Atom) {
            throw TypeError.forArgument(
                context,
                signature,
                TypeError.Expected.ATOM,
                it,
                index
            )
        }
    }
    return this
}

fun <C : ExecutionContext> Solve.Request<C>.throwPrologError(message: String?, category: RuntimeError.Category): Nothing =
    if (message != null) {
        throw RuntimeError.of(context, signature, category, message)
    } else {
        throw RuntimeError.of(context, signature, category, "runtime_error")
    }

inline fun <C : ExecutionContext, Req : Solve.Request<C>, R> Req.catchingMLRuntimeExceptions(action: Req.() -> R): R {
    try {
        return action()
    } catch (e: Exception) {
        when (e) {
            is DatasetException -> throwPrologError(e.message, RuntimeError.Category.DATASET)
            is TransformProcessException -> throwPrologError(e.message, RuntimeError.Category.TRANSFORM_PROCESS)
            is NetworkException -> throwPrologError(e.message, RuntimeError.Category.NETWORK)
            is SchemaDefinitionException -> throwPrologError(e.message, RuntimeError.Category.SCHEMA)
            else -> throw e
        }
    }
}
