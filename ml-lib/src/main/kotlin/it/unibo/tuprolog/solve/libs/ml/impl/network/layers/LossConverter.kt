package it.unibo.tuprolog.solve.libs.ml.impl.network.layers

import it.unibo.tuprolog.solve.libs.ml.LogicLoss
import org.nd4j.linalg.lossfunctions.ILossFunction
import org.nd4j.linalg.lossfunctions.LossFunctions
import org.nd4j.linalg.lossfunctions.impl.LossMCXENT
import org.nd4j.linalg.lossfunctions.impl.LossMSE

object LossConverter {

    fun implToLogic(dl4jLoss: ILossFunction): LogicLoss {
        return when (dl4jLoss) {
            is LossMSE -> LogicLoss.MSE
            is LossMCXENT -> LogicLoss.CROSS_ENTROPY
            else -> LogicLoss.OTHER
        }
    }

    fun logicToImpl(loss: LogicLoss): LossFunctions.LossFunction {
        return when (loss) {
            LogicLoss.MSE -> LossFunctions.LossFunction.MSE
            LogicLoss.CROSS_ENTROPY -> LossFunctions.LossFunction.MCXENT
            else -> TODO()
        }
    }
}
