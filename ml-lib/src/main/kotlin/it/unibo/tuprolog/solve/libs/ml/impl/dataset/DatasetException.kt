package it.unibo.tuprolog.solve.libs.ml.impl.dataset

class DatasetException(message: String) : Exception(message)
