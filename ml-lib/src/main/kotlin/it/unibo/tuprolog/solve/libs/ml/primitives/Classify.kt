package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.core.Substitution
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.libs.ml.LogicNetwork
import it.unibo.tuprolog.solve.libs.ml.impl.schema.StopQueryException
import it.unibo.tuprolog.solve.libs.oop.ObjectRef
import it.unibo.tuprolog.solve.primitive.QuaternaryRelation
import it.unibo.tuprolog.solve.primitive.Solve
import it.unibo.tuprolog.unify.Unificator.Companion.mguWith
import it.unibo.tuprolog.core.List as LogicList

/**
classify/4
Description = classify(Input , Network, ClassList, Label)
classify(+struct, -network, +list, -atom)
The result is the conversion of input* in a vector and the computation of Network output O with the vector as Input, label L is the atom A in ClassList which index correspond to the index of the output neuron with maximum value.
The substitution {Label/L} is returned.
Error cases
Input -> instantation_error, runtime_error*
Network -> instantation_error, type_error(object_ref), instance_type_error(nework)
ClassList -> instantation_error, type_error(list),  type_error(atom)
Label -> type_error(var)
Runtime errors -> runtime_error
* Input must be a struct with any functor and arity equal to number of input neurons, arg must be structs with arity1 and a numeric arg. If Input doesn’t match a runtime_error is returned
**/
object Classify : QuaternaryRelation.Functional<ExecutionContext>("classify") {

    override fun Solve.Request<ExecutionContext>.computeOneSubstitution(
        first: Term,
        second: Term,
        third: Term,
        fourth: Term
    ): Substitution {
        ensuringArgumentIsInstantiated(0)
        ensuringArgumentIsInstantiated(0)
        ensuringArgumentIsInstantiatedList(2)
        ensuringArgumentIsNetwork(1)
        ensuringArgumentIsAtomList(2)
        ensuringArgumentIsVar(3)
        val network = (second as ObjectRef).`object` as LogicNetwork
        val classList = (third as LogicList).toList().map { it.toString() }
        return try {
            catchingMLRuntimeExceptions {
                val result = network.classify(first as Struct, classList)
                return fourth mguWith Atom.of(result)
            }
        } catch (e: StopQueryException) {
            Substitution.failed()
        }
    }
}
