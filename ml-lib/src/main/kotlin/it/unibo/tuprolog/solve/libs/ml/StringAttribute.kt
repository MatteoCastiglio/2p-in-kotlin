package it.unibo.tuprolog.solve.libs.ml

import it.unibo.tuprolog.core.Atom
import it.unibo.tuprolog.core.Term

data class StringAttribute(override val index: Int, override val name: String) : Attribute {

    override val type: String
        get() = "String"

    override val default: Term
        get() = Atom.of("default")
}
