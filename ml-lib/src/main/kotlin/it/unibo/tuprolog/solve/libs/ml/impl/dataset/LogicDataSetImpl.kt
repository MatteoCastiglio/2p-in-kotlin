package it.unibo.tuprolog.solve.libs.ml.impl.dataset

import it.unibo.tuprolog.core.Struct
import it.unibo.tuprolog.solve.libs.ml.LogicDataSet
import it.unibo.tuprolog.solve.libs.ml.LogicSchema
import org.datavec.api.records.reader.RecordReader
import org.datavec.api.records.reader.impl.collection.CollectionRecordReader
import org.datavec.api.records.reader.impl.csv.CSVRecordReader
import org.datavec.api.split.FileSplit
import org.datavec.api.writable.Writable
import org.nd4j.linalg.api.rng.DefaultRandom
import java.io.File

class LogicDataSetImpl(val recordReader: RecordReader, private val name: String) : LogicDataSet, Iterator<List<Writable>> {

    private fun nextWithNull(): List<Writable>? {
        return if (recordReader.hasNext()) {
            recordReader.next()
        } else {
            null
        }
    }

    override fun theory(name: String, schema: LogicSchema): Sequence<Struct> {
        reset()
        return generateSequence {
            nextWithNull()
        }.map { RecordConverter.structFromRecord(it, name, schema) }
    }

    fun reset() {
        recordReader.reset()
    }

    override fun hasNext(): Boolean {
        return recordReader.hasNext()
    }

    override fun split(ratio: Double): Pair<LogicDataSet, LogicDataSet> {
        val rng = DefaultRandom()
        val trainFile = name.substringBeforeLast(".") + "Split1.csv"
        val testFile = name.substringBeforeLast(".") + "Split2.csv"
        val trainWriter = CSVUtils.createCSVWriter(trainFile)
        val testWriter = CSVUtils.createCSVWriter(testFile)
        while (hasNext()) {
            val record = next()
            if (rng.nextDouble() < ratio) {
                trainWriter.write(record)
            } else {
                testWriter.write(record)
            }
        }
        trainWriter.close()
        testWriter.close()
        return Pair(fromCsv(trainFile), fromCsv(testFile))
    }

    override fun next(): List<Writable> {
        return recordReader.next()
    }

    fun nextBatch(size: Int): List<List<Writable>> {
        var count = 0
        val processedData = mutableListOf<List<Writable>>()
        while (hasNext() && count < size) {
            processedData.add(recordReader.next())
            count += 1
        }
        return processedData
    }

    companion object {

        fun fromCsv(fileName: String): LogicDataSetImpl =
            try {
                val recordReader = CSVRecordReader()
                val fileSplit = FileSplit(File(fileName))
                recordReader.initialize(fileSplit)
                LogicDataSetImpl(recordReader, fileName)
            } catch (e: Exception) {
                throw(DatasetException(e.message!!))
            }

        fun fromStruct(struct: Struct): LogicDataSetImpl {
            val reader = CollectionRecordReader(listOf(RecordConverter.recordFromStruct(struct)))
            return LogicDataSetImpl(reader, "theory")
        }

        fun fromTheory(theory: Iterable<Struct>): LogicDataSetImpl =
            try {
                fromRecordCollection(theory.map { RecordConverter.recordFromStruct(it) })
            } catch (e: Exception) {
                throw(DatasetException("theory can't be converted to a dataset"))
            }

        private fun fromRecordCollection(records: List<List<Writable>>): LogicDataSetImpl {
            val reader = CollectionRecordReader(records)
            return LogicDataSetImpl(reader, "theory")
        }
    }
}
