package it.unibo.tuprolog.solve.libs.ml.primitives

import it.unibo.tuprolog.core.Scope
import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.ExecutionContext
import it.unibo.tuprolog.solve.rule.RuleWrapper

object Train4 : RuleWrapper<ExecutionContext>("train", 4) {
    override val Scope.head: List<Term>
        get() = kotlin.collections.listOf(varOf("N"), varOf("D"), varOf("Index"), varOf("Epochs"))

    override val Scope.body: Term
        get() = structOf("train", varOf("N"), varOf("D"), varOf("Index"), varOf("Index"), varOf("Epochs"))
}
