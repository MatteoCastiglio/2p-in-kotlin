package it.unibo.tuprolog.solve.libs.ml.impl.schema

import it.unibo.tuprolog.core.Term
import it.unibo.tuprolog.solve.libs.ml.Attribute
import it.unibo.tuprolog.solve.libs.ml.CategoricalAttribute
import it.unibo.tuprolog.solve.libs.ml.DoubleAttribute
import it.unibo.tuprolog.solve.libs.ml.IndexAttribute
import it.unibo.tuprolog.solve.libs.ml.IntegerAttribute
import it.unibo.tuprolog.solve.libs.ml.LogicSchema
import it.unibo.tuprolog.solve.libs.ml.StringAttribute
import org.datavec.api.transform.ColumnType
import org.datavec.api.transform.metadata.CategoricalMetaData
import org.datavec.api.transform.schema.Schema as D4JSchema

class LogicSchemaImpl(val schema: D4JSchema) : LogicSchema {

    private var index: String? = null
    internal var transformProcess: LogicTransformProcessImpl = LogicTransformProcessImpl(this, this, needFitting = false)

    fun describe(): String {
        return "Number of columns: " + schema.numColumns() + System.lineSeparator() +
            "Column names: " + schema.columnNames + System.lineSeparator() + "Column types: " + schema.columnTypes
    }

    override fun attributesNames(): List<String> =
        attributes().map { it.name }

    override fun attributes(): List<Attribute> =
        schema.columnMetaData.mapIndexed { count, column ->
            when (column.columnType) {
                ColumnType.Integer ->
                    if (index == column.name) {
                        IndexAttribute(count, column.name)
                    } else {
                        IntegerAttribute(count, column.name)
                    }
                ColumnType.Double -> DoubleAttribute(count, column.name)
                ColumnType.String -> StringAttribute(count, column.name)
                ColumnType.Categorical -> {
                    val metadata = column as CategoricalMetaData
                    CategoricalAttribute(count, metadata.name, metadata.stateNames)
                }
                else -> TODO()
            }
        }

    override fun logicList(): it.unibo.tuprolog.core.List =
        Attribute.logicList(attributes())

    override fun termList(): List<Term> =
        Attribute.termList(attributes())

    companion object {

        private fun getColumn(attribute: Attribute, builder: D4JSchema.Builder) {
            when (attribute) {
                is IntegerAttribute -> builder.addColumnInteger(attribute.name)
                is DoubleAttribute -> builder.addColumnDouble(attribute.name)
                is IndexAttribute -> builder.addColumnInteger(attribute.name)
                is CategoricalAttribute -> builder.addColumnCategorical(
                    attribute.name,
                    attribute.categories
                )
                is StringAttribute -> builder.addColumnString(attribute.name)
            }
        }

        fun of(attributes: List<Attribute>): LogicSchemaImpl {
            val schemaBuilder = D4JSchema.Builder()
            attributes.forEach { getColumn(it, schemaBuilder) }
            val schema = schemaBuilder.build()
            val schemaProxy = LogicSchemaImpl(schema)
            try {
                schemaProxy.index = attributes.first { it is IndexAttribute }.name
            } catch (_: NoSuchElementException) {
                // do nothing
            }
            return schemaProxy
        }
    }
}
