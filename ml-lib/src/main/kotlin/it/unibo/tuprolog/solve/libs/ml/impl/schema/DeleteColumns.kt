package it.unibo.tuprolog.solve.libs.ml.impl.schema

import org.datavec.api.transform.TransformProcess
import org.datavec.api.transform.analysis.DataAnalysis

class DeleteColumns(private val columns: List<String>) : SchemaOperation(), PipelineElement {

    override fun operation(builder: TransformProcess.Builder, da: DataAnalysis) {
        builder.removeColumns(columns)
    }
}
