import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val javaVersion: String by project
val ktFreeCompilerArgsJvm: String by project

dependencies {
    api(kotlin("stdlib-jdk8"))
    api(project(":oop-lib"))
    implementation("org.deeplearning4j:deeplearning4j-core:_")
    implementation("org.datavec:datavec-local:_")
    runtimeOnly("org.nd4j:nd4j-native-platform:_")
    runtimeOnly("org.slf4j:slf4j-nop:_")
    api(project(":dsl-theory"))
    api(project(":solve-classic"))
    // testImplementation(project(":dsl-theory"))
    // testImplementation(project(":solve-classic"))
    testImplementation(project(":test-solve"))
    testImplementation(kotlin("test-junit"))
}

configure<JavaPluginConvention> {
    targetCompatibility = JavaVersion.valueOf("VERSION_1_$javaVersion")
    sourceCompatibility = JavaVersion.valueOf("VERSION_1_$javaVersion")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.$javaVersion"
        freeCompilerArgs = ktFreeCompilerArgsJvm.split(";").toList()
    }
}
